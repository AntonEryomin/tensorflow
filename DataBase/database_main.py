from datetime import datetime
from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError
from Core.Configuration.configuration import Configuration


class MongoDataBase(object):
    def __init__(self):
        config = Configuration()
        self.name = config.db_name
        self.host = config.db_host
        self.port = config.db_port

    def create(self):
        client = MongoClient()
        db = client['Lamoda']
        db.Lamoda.insert_one({'Create': datetime.now().strftime('%Y-%m-%d %H:%M:%S')})

    def is_run(self):
        try:
            client = MongoClient(self.host, serverSelectionTimeoutMS=2)
            if self.name not in client.database_names():
                return False
            return True
        except ServerSelectionTimeoutError as err:
            print(err)
            return False

    def ifCreate(self):
        pass

    def getDBparams(self):
        pass

    def setDBparam(self):
        pass

    def get_document(self):
        client = MongoClient()
        db = client['Lamoda']
        doc = db.Lamoda.find({"sku": "JP875GUAX842"})
        for d in doc:
            print(d)
        return doc

    def insert_document(self, inserted_document):
        client = MongoClient()
        db = client['Lamoda']
        db.Lamoda.insert_one(inserted_document)

