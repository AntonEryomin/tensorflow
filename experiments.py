import matplotlib.pyplot as plt
import numpy as np
from Core.Accuaracy.ConfusionMatrix import ConfusionMatrix
from sklearn.metrics import confusion_matrix
from Core.Results.Results_from_file import ResultsFromFile
from levinshtein_distance import Similarity


def get_statistics(list_data):
    statistics_dict = {}
    for data in list_data:
        if data not in statistics_dict:
            statistics_dict[data] = 1
        else:
            statistics_dict[data] = statistics_dict[data] + 1
    return statistics_dict


def get_only_classes(full_dict):
    labels = []
    for key in full_dict.keys():
        for subvalue in full_dict[key]:
            if subvalue not in labels:
                labels.append(subvalue)
    return labels


if __name__ == "__main__":
    all_results_from_file = ResultsFromFile(r'/media/antoneryomin/Новый том/WORK/TENSORFLOW/MODELS/16112017/benchmark_result.json')
    resFile = all_results_from_file.get_only_result_pares()
    to_plot = resFile['normal']
    only_classes = []
    for list_elem in to_plot:
        for sub_list in list_elem:
            if sub_list not in only_classes:
                only_classes.append(sub_list)
    results_dict = {}
    for shoes_name in only_classes:
        if shoes_name not in results_dict.keys():
            results_dict[shoes_name] = {}
        for pare in to_plot:
            if shoes_name == pare[0] and shoes_name == pare[1]:
                if len(results_dict[shoes_name]) == 0:
                    results_dict[shoes_name]['diagonal'] = 1
                else:
                    results_dict[shoes_name]['diagonal'] += 1
        try:
            if results_dict[shoes_name]['diagonal'] < 60:
                results_dict.pop(shoes_name)
        except:
            results_dict.pop(shoes_name)
    trusted_pare = []
    for trusted_classes in results_dict.keys():
        for pare in to_plot:
            if pare[0] == trusted_classes:
                if pare[1] != trusted_classes:
                    if tuple(pare) in results_dict[trusted_classes].keys():
                        results_dict[trusted_classes][tuple(pare)] += 1
                    else:
                        results_dict[trusted_classes][tuple(pare)] = 1
        for pare in results_dict[trusted_classes].keys():
            if pare != 'diagonal':
                if results_dict[trusted_classes][tuple(pare)] / results_dict[trusted_classes]['diagonal'] < 0.05:
                    trusted_pare.append(pare)
            # else:
            #     trusted_pare.append((trusted_classes, trusted_classes))

    to_analyze = ResultsFromFile(r'/media/antoneryomin/Новый том/WORK/TENSORFLOW/MODELS/16112017/benchmark_result.json').get_normilized_data()['normal']
    to_check = []
    for text_path in to_analyze:
        for sku_recognize in text_path.keys():
            for sku in text_path[sku_recognize].keys():
                if tuple(text_path[sku_recognize][sku]) in trusted_pare:
                    to_check.append((sku, text_path[sku_recognize][sku]))

    #Формат вывода: class_label: [Tp, Tn, Fp, Fn, Precession, Recall, F];

    # accuracy_count = {}
    # for label in X_labels:ч
    #     accuracy_count[label] = [0, 0, 0, 0, 0, 0, 0]
    #     for pare in all_results_from_file.get_only_result_pares()['normal']:
    #         if label == pare[0] and label == pare[1]:
    #             accuracy_count[label][0] = accuracy_count[label][0] + 1
    #         elif label != pare[0] and label != pare[1]:
    #             accuracy_count[label][1] = accuracy_count[label][1] + 1
    #         elif label != pare[0] and label == pare[1]:
    #             accuracy_count[label][2] = accuracy_count[label][2] + 1
    #         elif label == pare[0] and label != pare[1]:
    #             accuracy_count[label][3] = accuracy_count[label][3] + 1
    #     try:
    #         accuracy_count[label][4] = accuracy_count[label][0] / (accuracy_count[label][0] + accuracy_count[label][2])
    #     except ZeroDivisionError:
    #         accuracy_count[label][4] = 0
    #     try:
    #         accuracy_count[label][5] = accuracy_count[label][0] / (accuracy_count[label][0] + accuracy_count[label][3])
    #     except ZeroDivisionError:
    #         accuracy_count[label][5] = 0
    #     try:
    #         accuracy_count[label][6] = 2*(accuracy_count[label][4] * accuracy_count[label][5]) / (accuracy_count[label][4] + accuracy_count[label][5])
    #     except ZeroDivisionError:
    #         accuracy_count[label][6] = 0
    #
    #
    #
    #
    #
    #
    # data_to_plot = {}
    # for class_ in X_labels:
    #     temp = [0, 0]
    #     if class_ in full_data['catalog'].keys():
    #         temp[0] = full_data['catalog'][class_]
    #     else:
    #         temp[0] = 0
    #     if class_ in full_data['predicted'].keys():
    #         temp[1] = full_data['predicted'][class_]
    #     else:
    #         temp[1] = 0
    #     data_to_plot[class_] = temp
    #     temp = [0, 0]
    #
    # N = len(data_to_plot)
    # ind = np.arange(N)
    # width = 0.15
    # fig, ax = plt.subplots()
    # catalog_means = []
    # predicted_means = []
    # precession = []
    # recall = []
    # f_mean = []
    # for label in X_labels:
    #     try:
    #         catalog_means.append(full_data['catalog'][label])
    #     except KeyError:
    #         catalog_means.append(0)
    #     try:
    #         predicted_means.append(full_data['predicted'][label])
    #     except KeyError:
    #         predicted_means.append(0)
    #     try:
    #         precession.append(accuracy_count[label][4])
    #     except KeyError:
    #         precession.append(0)
    #     try:
    #         recall.append(accuracy_count[label][5])
    #     except KeyError:
    #         recall.append(0)
    #     try:
    #         f_mean.append(accuracy_count[label][6])
    #     except KeyError:
    #         f_mean.append(0)
    # rects1 = ax.bar(ind, tuple(catalog_means), width, color='r')
    # rects2 = ax.bar(ind + width, tuple(predicted_means), width, color='y')
    #
    # ax.legend((rects1[0], rects2[0]), ('Catalog', 'Predicted'))
    # ax.set_xticks(np.arange(len(X_labels)))
    # ax.set_xticklabels(tuple(X_labels))
    # for tick in ax.get_xticklabels():
    #     tick.set_rotation(45)
    # plt.figure()
    # conf_matrix.plot_confusion_matrix(confusion_matrix(conf_matrix.data_to_confusion_library()[0], conf_matrix.data_to_confusion_library()[1], conf_matrix.get_classes(to_plot)), classes=conf_matrix.get_classes(to_plot))
    # plt.show()

    # print(precession)
    # print(recall)
    # print(f_mean)
    # rects3 = ax.bar(ind, tuple(precession), color='b')
    # rects4 = ax.bar(ind + width, tuple(recall), color='g')
    # rects5 = ax.bar(ind + 2 * width, tuple(f_mean), color='m')
    # ax.legend((rects3[0], rects4[0], rects5[0]), ('Precession', 'Recall', 'F_mean'))
    # ax.set_xticks(np.arange(len(X_labels)))
    # ax.set_xticklabels(tuple(X_labels))
    # for tick in ax.get_xticklabels():
    #     tick.set_rotation(45)
    # plt.show()

    # only_precision = []
    # only_recall = []
    # only_f_mean = []
    # constant_border_value = 0.5
    # for shoes_class in accuracy_count.keys():
    #     if accuracy_count[shoes_class][4] > constant_border_value:
    #         only_precision.append(shoes_class)
    #     if accuracy_count[shoes_class][5] > constant_border_value:
    #         only_recall.append(shoes_class)
    #     if accuracy_count[shoes_class][6] > constant_border_value:
    #         only_f_mean.append(shoes_class)
    # print(only_precision)
    # print(only_recall)
    # print(only_f_mean)
    #
    # plt.show()

    # only_precision = ['polusapogi', 'botforty', 'bosonozhki', 'krossovki', 'butsy', 'baletki', 'sandalii', 'dutiki', 'rezinovye polusapogi', 'botinki', 'sapogi', 'tufli', 'shipovki', 'kedy']
    # only_recall = ['slantsy', 'polusapogi', 'botforty', 'bosonozhki', 'krossovki', 'butsy', 'baletki', 'sandalii', 'botil ony', 'espadril i', 'dutiki', 'tapochki', 'mokasiny', 'botinki', 'lofery', 'sapogi', 'tufli', 'slipony', 'botinki trekingovye', 'kedy']
    # only_f_mean = ['polusapogi', 'botforty', 'bosonozhki', 'krossovki', 'butsy', 'baletki', 'sandalii', 'botil ony', 'dutiki', 'botinki', 'sapogi', 'tufli', 'kedy']
