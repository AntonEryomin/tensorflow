import tensorflow as tf
from Core.Pictures.picture import Picture
from Core.IRmodels.ir_model import IRmodel
from Core.Results.Result import Result


class Recognition(Picture):
    def __init__(self, picture, ir_model):
        self.picture = picture
        self.ir_model = ir_model

    @property
    def picture(self):
        return self._pic

    @picture.setter
    def picture(self, pic):
        if not isinstance(pic, Picture):
            raise TypeError
        self._pic = pic

    @property
    def ir_model(self):
        return self._ir_mod

    @ir_model.setter
    def ir_model(self, ir_mod):
        if not isinstance(ir_mod, IRmodel):
            raise TypeError
        self._ir_mod = ir_mod

    def recognition_run(self):
        image_data = tf.gfile.FastGFile(self.picture.path, 'rb').read()
        with tf.gfile.FastGFile(self.ir_model.path_to_pb, 'rb') as f:
            tf.reset_default_graph()
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            _ = tf.import_graph_def(graph_def, name='')
        with tf.Session() as sess:
            softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
            predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})
            top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
            attr_value = []
            for node_id in top_k:
                tempDict = {}
                human_string = self.ir_model.get_available_classes()[node_id]
                score = predictions[0][node_id]
                tempDict[human_string] = round(score.item(), 3)
                attr_value.append(tempDict)
        return Result(attr_value)
