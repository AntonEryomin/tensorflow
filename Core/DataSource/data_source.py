import requests
import json
from Core.Configuration.configuration import Configuration


class DataSource:
    def __init__(self, source, sku):
        self.source = source
        self.sku = sku.upper()

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, s):
        if not s:
            raise Exception("Source doesn't set.")
        if s not in ['apigw', 'catalogue', 'db']:
            raise Exception("Can't setting source.")
        self._source = s

    @property
    def sku(self):
        return self._sku

    @sku.setter
    def sku(self, s):
        if len(s) != 12:
            raise Exception("The SKU length is not equal 12.")
        self._sku = s

    def data_catalogue_link(self):
        return r'{0}{1}?country=ru'.format(Configuration().catalogue_server, self.sku)

    def get_data_apigw(self):
        config = Configuration().apigw_url
        json_to_work = requests.post(str(config), data=json.dumps({'product_sku': self.sku})).text
        return json.loads(json_to_work)

    def get_data_catalogue(self):
        sku_url = self.data_catalogue_link()
        try:
            json_data = requests.get(sku_url).json()['data']
            return json_data
        except requests.exceptions.HTTPError as err:
            print(err)
        except ConnectionError as err:
            print(err)
        except requests.ConnectionError as err:
            print(err)

    def get_data_db(self):
        pass