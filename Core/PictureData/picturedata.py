# -*- coding: utf-8 -*-
"""
Класс предоставляющий объект, для записи в файл, а также считывания строки из файла и преобразования его в объекты
соответствующих классов.
"""
import requests
import transliterate as tr


class PictureData:
    KEYS_TO_BE_DELETED = ('discount', 'labels', 'sizes', 'owner_id', 'old_price_amount', 'seller',
                          'category_leaves_denormalized', 'sortings')
    PROHIBIT_LIST_KEYS = ['thumbnail', 'url', 'discount', 'description', 'sortings', 'old_price_amount', 'video',
                          'price_iso_code', 'price_amount', 'sku_supplier', 'size_system', 'sizes', 'is_gift',
                          'labels', 'is_new', 'gallery', 'is_sellable', 'id', 'company_name', 'shop_name', 'image',
                          'seller', 'look_skus', 'rollover', 'photo_sprite', 'seo_tail', 'collection',
                          'category_leaves_denormalized', 'production_country', 'stock_type', 'material_filling',
                          'season', 'model_parameters', 'model_height_on_photo', 'start_season', 'correspond_to_size',
                          'upper_material_supplier', 'lining_material', 'model_name', 'site_section', 'side_length',
                          'inside_leg_length', 'height', 'bag_bottom_width', 'handle_length', 'width', 'length',
                          'technology', 'is_outlet', 'color_family', 'sleeve_length', 'sleeve', 'shoulder_strap',
                          'a4_format', 'shaft_height_exact', 'shaft_height_exact', 'inner_sole_material',
                          'upper_material', 'pockets', 'style', 'num_pockets', 'inside_material', 'textile_trikotaj',
                          'fabric', 'sole_material_supplier', 'thigh_girth', 'bottom_width', 'waist_girth',
                          'material_filler', 'inner_material_supplier', 'volume', 'aroma_type', 'skin_type',
                          'accs_length', 'bag_width', 'sole_material', 'bag_height', 'distance_between_the_arches',
                          'closed_length', 'the_diameter_of_the_umbr', 'volume_bags', 'lining', 'hood',
                          'heel_height_exact', 'platform_height_exact', 'shaft_width_exact', 'diameter_of_dial',
                          'spike_amount', 'membrana', 'uv_protection', 'light_transmission', 'the_number_den',
                          'age_type', 'makeup_colour', 'promo', 'sku']

    def __init__(self, picture_obj, picture_descr_obj, picture_source):
        """
        Метод для инициализации объекта PictureData
        :param picture_obj: объект класса Pictures
        :param picture_descr_obj: обеъкт класса Pictures Description
        :param picture_source: источник данных, может быть либо api каталога, либо база данных
        """
        self.picture = picture_obj
        self.picture_descr_obj = picture_descr_obj
        self.source = picture_source

    def get_picture_data_from_source(self):
        """
        Основной метод для получения данных о картинке (читай SKU).
        :return: словарь полученных данных
        """
        if self.source == 'catalogue':
            url_for_current_pict = 'http://catalog.services.lamoda.tech/products/' + self.picture.sku + '?country=ru'
            try:
                json_data = requests.get(url_for_current_pict).json()
                return json_data
            except requests.exceptions.HTTPError as err:
                print(err)
            except ConnectionError as err:
                print(err)
            except requests.ConnectionError as err:
                print(err)

    def get_gender_name_data_from_path(self):
        return (self.picture.path.split('\\')[2], self.picture.path.split('\\')[3])

    def get_data_dict(self):
        """
        Метод для данных с каталога, полученные данные заворачиваем в словарь. Если из исходного источника данных, мы
        не находим искомый аттрибут, тогда мы проставялем значение default, для такого атрибута.
        :return: словарь с данными, словарь содержит следующие ключи:
        type
        name
        gender
        brand
        attr_clasp
        attr_type_of_heels
        attr_type_of_platform
        """
        data_dict = {}
        try:
            full_json_data = self.get_picture_data_from_source()['data']
            if "type" in full_json_data.keys():
                data_dict['type'] = full_json_data['type'].lower()
            else:
                data_dict['type'] = "default"
            if "name" in full_json_data.keys():
                data_dict["name"] = full_json_data['name'].lower()
            else:
                data_dict["name"] = "default"
            if "gender" in full_json_data.keys():
                data_dict['gender'] = full_json_data['gender'].lower()
            else:
                data_dict["gender"] = "default"
            if 'brand' in full_json_data.keys():
                data_dict['brand'] = full_json_data['brand']['name'].lower()
            else:
                data_dict["brand"] = "default"
            if 'attributes' in full_json_data.keys():
                attr_json = full_json_data['attributes']
                if 'clasp' in attr_json.keys():
                    data_dict['clasp'] = attr_json['clasp'].lower()
                else:
                    data_dict["clasp"] = "default"
                if 'type_of_heels' in attr_json.keys():
                    data_dict['type_of_heels'] = attr_json['type_of_heels']
                else:
                    data_dict['type_of_heels'] = 'default'
            return data_dict
        except AttributeError:
            return {'data': "No data for SKU: {0}".format(self.picture.sku)}
        except TypeError as err:
            print(err)
            return {'data': "No data for SKU: {0}".format(self.picture.sku)}

    def data_to_write(self):
        """
        Метод для записи данных о картинки в файл. На первой итерации записывается, name, gender, clasp и платформа.
        :return: список данных, которые необходимо записать
        """
        data_list = []
        data_from_path = self.get_gender_name_data_from_path()
        if data_from_path[0] is not None:
           data_list.append(data_from_path[0])
        if data_from_path[1] is not None:
            if tr.detect_language(data_from_path[1]) == 'ru':
                data_list.append(tr.translit(value=data_from_path[1], reversed=True))
            else:
                data_list.append(data_from_path[1])
        json_data = self.get_picture_data_from_source()
        try:
            if json_data['data']['attributes'] is not None:
                if 'clasp' in json_data['data']['attributes']:
                    if tr.detect_language(json_data['data']['attributes']['clasp']) == 'ru':
                        data_list.append(tr.translit(value=json_data['data']['attributes']['clasp'], reversed=True))
                    else:
                        data_list.append(json_data['data']['attributes']['clasp'])
                if 'type_of_heels' in json_data['data']['attributes'].keys():
                    list_types = json_data['data']['attributes']['type_of_heels'].split(',')
                    for elem in list_types:
                        if tr.detect_language(elem) == 'ru':
                            data_list.append(tr.translit(value=elem, reversed=True))
                        else:
                            data_list.append(elem)
        except TypeError:
            raise
        return data_list

    def to_file(self, path_to_file):
        """
        Метод для записи объекта в файл.
        :return:
        """
        try:
            with open(path_to_file, 'a') as f:
                for data_to_write in self.data_to_write():
                    f.write(data_to_write + '\n')
        except FileExistsError as e:
            print(e)