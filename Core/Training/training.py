import os
import shutil
import subprocess
import Core.Classes.DataManipulation.datafromfiles as dff
from Core.Configuration import properties as prop


pic_folder_path = 'D:\\IR\\FOR_LIST_TEST'

# ----------------------------------------------------------------------------------------------------------------------
# Step 1.
# Methods for output results
# ----------------------------------------------------------------------------------------------------------------------
def get_all_created_models():
    with open('D:\\IR\\' + 'AVAILABLE_MODELS.txt', 'w') as file:
        for categ in dff.get_path_to_categ_files().keys():
            file.write('{0}: {1}\n'.format(categ, os.path.basename(dff.get_path_to_categ_files()[categ][1])))


# ----------------------------------------------------------------------------------------------------------------------
# Step 2.
# Get all attributes from folder InputFiles
# ----------------------------------------------------------------------------------------------------------------------
def main():
    for dir in os.listdir(pic_folder_path):
        listVariables = [1, 2, 3]
        if len(os.listdir(pic_folder_path + "\\" + dir)) > 1:
            pathToMainFolder = pic_folder_path + "\\" + dir + '\\'
            bottleneck_dir = pathToMainFolder + 'bottlenecks\\'
            if not os.path.exists(bottleneck_dir):
                os.makedirs(bottleneck_dir)
            model_dir = prop.PATH_TO_PICTURES_FOLDER + '\\inception'
            if not os.path.exists(model_dir):
                os.makedirs(model_dir)
            output_graph = pathToMainFolder + '{0}_graph.pb'.format(dir.lower())
            output_labels = pathToMainFolder + '{0}_labels.txt '.format(dir.lower())
            image_dir = pathToMainFolder
            subprocess.call(
                'C:\\Users\\anton.eremin\\AppData\\Local\\Programs\\Python\\Python35\\python.exe {0} --bottleneck_dir={1} --how_many_training_steps {2} --model_dir={3} --output_graph={4} --output_labels={5} --image_dir={6}'.format(
                    prop.PATH_TO_TRAIN_SCRIPT, bottleneck_dir, prop.TRAINING_STEPS, model_dir, output_graph,
                    output_labels, image_dir),
                shell=True)


def folder_checker():
    start_folder = [folder + '\\' for folder in os.listdir(os.path.join(pic_folder_path))]
    folder_to_delete = []
    for rootFolder in start_folder:
        if rootFolder != '.DS_Store/':
            checked_folder = os.path.join(pic_folder_path, rootFolder)
            for file in os.listdir(checked_folder):
                if os.path.isdir(file) == True:
                    if len(os.listdir(os.path.join(checked_folder, file))) < 21:
                        folder_to_delete.append(rootFolder + checked_folder)
    if len(folder_to_delete) > 0:
        with open(prop.PATH_TO_PICTURES_FOLDER + 'deletedFolder.txt', 'w') as file_handler:
            for item in folder_to_delete:
                file_handler.write("{}\n".format(item))
    for folder in folder_to_delete:
        shutil.rmtree(pic_folder_path + folder)


# ----------------------------------------------------------------------------------------------------------------------
# Run training process.
# ----------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    folder_checker()

    main()
    get_all_created_models()
