# -*- coding: utf-8 -*-
"""
Класс для работы с результатом распознавания картинки. Описание ключе класса следующее:
pict_path - путь до картинки, которая распознавалась
marked_data - данные которые были где-то размечены
"""
import os
import json
from Core.Configuration.properties import DIRECTORY_RESULTS

transformed_dict = {}


class Result:
    transformed_dict = {}

    def __init__(self, recognized_dictionary):
        self.recognizedDictionary = recognized_dictionary

    def mediana_calculation(self, ordered_list_values):
        """
        Метод для вычисления медианы, значение медианы вычилсяется для упорядоченного списка.
        :return: значение медианы
        """
        if len(ordered_list_values) == 2:
            return sum(ordered_list_values) / 2
        else:
            if len(ordered_list_values) % 2 == 0:
                return ordered_list_values[len(ordered_list_values) // 2] + ordered_list_values[(len(ordered_list_values) // 2) + 1] / 2
            else:
                return ordered_list_values[len(ordered_list_values) // 2]

    def result_to_ordered_dict(self):
        """
        Метод для преобразования исходного словаря результатов в словарь результатов, с упорядоченным значением.
        :return: словарь с упорядоченными значениями
        """
        updated_dict = {}
        for key, value in self.recognizedDictionary.items():
            sorted_values = sorted(self.recognizedDictionary[key].values(), reverse=True)
            sorted_list = []
            for i in range(0, len(sorted_values)):
                for key_dict, value_dict in self.recognizedDictionary[key].items():
                    if value_dict == sorted_values[i]:
                        sorted_list.append((key_dict, value_dict))
            updated_dict[key] = sorted_list
        return updated_dict

    def result_to_ordered_dict_mediana(self):
        """
        Метод для преобразования исходного словаря результатов в словарь результатов, с упорядоченным значением с учетом
         медианы.
        :return: словарь с упорядоченными значениями
        """
        updated_dict = {}
        for key, value in self.recognizedDictionary.items():
            sorted_values = sorted(self.recognizedDictionary[key].values(), reverse=True)
            mediana = self.mediana_calculation(sorted_values)
            sorted_list = []
            for i in range(0, len(sorted_values)):
                for key_dict, value_dict in self.recognizedDictionary[key].items():
                    if value_dict == sorted_values[i]:
                        sorted_list.append((key_dict, value_dict/mediana))
            updated_dict[key] = sorted_list
        return updated_dict

    def final_probability(self):
        """
        Метод для подсчитывания итоговой вероятности. Считается как произведение вероятности всех событий.
        :return: итоговую вероятность.
        """
        ordered_dict = self.result_to_ordered_dict_mediana()
        final_probability = 1
        for attr in ordered_dict.keys():
            final_probability = final_probability * ordered_dict[attr][0][1]
        return final_probability

    def dict_final_verdict_without_mediana(self):
        """
        Метод для сортировки словаря в порядке убывания вероятностей внутри подклассов, без учета медианного значения.
        :return: отсортированный словарь
        """
        sorted_dict = {}
        for key, value in self.recognizedDictionary.items():
            sorted_values = sorted(self.recognizedDictionary[key].values(), reverse=True)
            sorted_list = []
            for i in range(0, len(sorted_values)):
                for key_dict, value_dict in self.recognizedDictionary[key].items():
                    if value_dict == sorted_values[i]:
                        sorted_list.append((key_dict, value_dict))
            sorted_dict[key] = sorted_list
        final_probability = 1
        for attr in sorted_dict.keys():
            final_probability = final_probability * sorted_dict[attr][0][1]
        return final_probability

    def dict_final_verdict(self):
        updated_dict = self.result_to_ordered_dict_mediana()
        dict_final_verdict = {}
        for key in updated_dict.keys():
            if updated_dict[key][0][1] > 2:
                dict_final_verdict[key] = (updated_dict[key][0][0], '100% sure')
            elif updated_dict[key][0][1] <= 2 and updated_dict[key][0][1] > 1:
                dict_final_verdict[key] = (updated_dict[key][0][0], '50% sure')
            else:
                dict_final_verdict[key] = (updated_dict[key][0][0], "not sure")
        return dict_final_verdict

    def dict_with_normilize(self):
        """
        Метод для нормализации значений внутри подгрупп.
        :return: словарь, где значение
        """
        normalized_dict = {}
        for key, value in self.recognizedDictionary.items():
            normalized_subdict = {}
            normilized_value = sum([float(value) for key, value in value.items()])
            for subdict_key, subdict_value in value.items():
                normalized_subdict[subdict_key] = subdict_value/normilized_value
            normalized_dict[key] = normalized_subdict
        return normalized_dict

    @staticmethod
    def _recognized_value_to_normal(value):
        """
        !!!ВАЖНО!!!
        !!!МЕТОД ВРЕМЕННЫЙ ПОТОМ ЕГО НЕЛЬЗЯ ИСПОЛЬЗОВАТЬ!!!
        Метод для преобразования результатов к унифицированному значению.
        :param value: значение, которое необходимо проверить
        :return: обновленное значение, если данное значение необходимо преобразовать, иначе возвращает тоже самое
        значнеие.
        """
        if value in transformed_dict.keys():
            return transformed_dict[value]
        else:
            return value

    @staticmethod
    def _tuple_value_to_normal(tuple_value):
        return (Result._recognized_value_to_normal(tuple_value[0]), tuple_value[1])

    @staticmethod
    def write_results_to_json(file_path, result_as_dict):
        """
        Метод для записи объекта класс в json файл.
        :param file_path: путь до файла, в который будет сохраняться объект
        :return: None
        """
        full_path_to_file = os.path.join(DIRECTORY_RESULTS, file_path)
        try:
            if os.path.exists(full_path_to_file):
                with open(full_path_to_file, 'r') as f:
                    current_data = json.load(f)
                    print(current_data)
                current_data[[key for key in result_as_dict.keys()][0]] = result_as_dict[[key for key in result_as_dict.keys()][0]]
            else:
                current_data = result_as_dict

            with open(full_path_to_file, 'w') as f:
                json.dump(current_data, f, indent=4)
        except FileExistsError as err:
            print(err)
            print("Can't create file {0}".format(file_path))

    @staticmethod
    def get_only_winner(ordered_dict, with_prob_value=False):
        """
        Метод для сохраения в распознанном словаре только победителей.
        ВАЖНО!!! исходный объект словаря должен быть отсортирован.
        :return: словарь где содержаться только победители
        """
        final_dict = {}
        for key in ordered_dict.keys():
            if with_prob_value == False:
                final_dict[key] = Result._tuple_value_to_normal(ordered_dict[key][0])
            else:
                final_dict[key] = Result._tuple_value_to_normal(ordered_dict[key][0][0])
        return final_dict

    @staticmethod
    def compare_with_marked_dict(recognized_dict, marked_dict):
        """
        Метод для получения словаря вроятности схожести с каталогом. Значение из каталога сравнивается со значением,
        полученным после распознавания изображения.
        :param marked_dict:
        :return: словарь значений.
        """
        updated_dict = {}
        for marked_key, marked_value in marked_dict.items():
            if marked_value in transformed_dict.keys():
                marked_value = transformed_dict[marked_value]
            winner_value = recognized_dict[marked_key][0][1]
            for elem in recognized_dict[marked_key]:
                if elem[0] == marked_value:
                    recognized_value = elem[1] / winner_value
                    updated_dict[marked_key] = (marked_value, recognized_value)
                    break
        return updated_dict

    @staticmethod
    def compare_with_marked_dict_without_prob(recognized_dict, marked_dict):
        """
        Метод для сравнения
        :param recognized_dict:
        :param marked_dict:
        :return:
        """
        diff_dict = {}
        for key, value in recognized_dict.items():

            if key not in marked_dict.keys():
                diff_dict[key] = ("Catalog: no value", "Model: " + value[0])
            elif marked_dict[key] != value[0]:
                diff_dict[key] = ("Catalog: " + marked_dict[key], "Model: " + value[0])
        return diff_dict

    @staticmethod
    def full_result(sku_id, pict_path_dicts_list):
        """
        Метод для получения финального результата распознавания. Результат выводится в виде SKU_ID: [SKU_PICT_1_PATH:
        {CATALOG_DICT: value, RECOGNIZED_DICT: value, TRUSTED_DICT: value},  SKU_PICT_2_PATH: {CATALOG_DICT: value,
        RECOGNIZED_DICT: value, TRUSTED_DICT: value} ... SKU_PICT_N_PATH: {CATALOG_DICT: value, RECOGNIZED_DICT: value,
        TRUSTED_DICT: value}]
        :param sku_id: идентификатор SKU
        :param pict_path_dicts_list: список словарей связанных с конкретным изображение. Словарь CATALOG_DICT -
        представляет собой данные об SKU, взятых из каталога, так же в нашей нотации это marked_dict, RECOGNIZED_DICT -
        представляет собой распознанные данные о картинке, те это фактически то, что мы получаем после распознавания
        изображения, TRUSTED_DICT - словарь данных в которых приводят лишь соотношение вероятности "победителя" со
        значением из каталога.
        :return:
        """
        return {sku_id: [pict_dict for pict_dict in pict_path_dicts_list]}

    @staticmethod
    def accuracy_score(dict_results):
        """
        Метод для вычисления точности прогноза.
        :param dict_results: словарь, содержащий результаты распознавания, а также вероятности по каждой из них.
        :return:
        """
        score_coefficient = {'name': 0.4, 'gender': 0.3, 'clasp': 0.15, 'platforma': 0.05,
                             'kabluk': 0.1}
        final_score = 0
        for key_score, value_score in dict_results.items():
            if key_score in score_coefficient.keys():
                final_score = float(value_score[1])*float(score_coefficient[key_score]) + float(final_score)
        return final_score