class Result:
    def __init__(self, results_list):
        self.results_list = results_list

    @property
    def results_list(self):
        return self._res_list

    @results_list.setter
    def results_list(self, res_list):
        if len(res_list) <= 0:
            raise Exception("Results list is empty.")
        self._res_list = res_list

    def _sorted_results(self):
        def partition(array, p, r):
            x = array[r]
            i = p - 1
            for j in range(p, r):
                if [elem for elem in array[j].values()][0] <= [elem for elem in x.values()][0]:
                    i = i + 1
                    array[i], array[j] = array[j], array[i]
            array[i+1], array[r] = array[r], array[i+1]
            return i+1

        def quick_sort(array, p, r):
            if p < r:
                q = partition(array, p, r)
                quick_sort(array, p, q-1)
                quick_sort(array, q+1, r)
            return array
        return quick_sort(self.results_list, 0, len(self.results_list)-1)

    def max_result_value(self):
        return self._sorted_results()[-1]

    def min_result_value(self):
        return self._sorted_results()[0]

    def get_top_n(self, n):
        return self._sorted_results()[-1:-int(n)-1:-1]