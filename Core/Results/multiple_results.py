# -*- coding: utf-8 -*-
import os
import csv
import tensorflow as tf
import transliterate as tr
from Core.Pictures.picture import Picture
from Core.Pictures.lamoda_picture import LamodaPicture
import Core.download_data_for_sku as ddfs
from Core.Pictures.picture_info import PictureInfo
from Core.Results.Result_old import Result
from Core.PictureData.picturedata import PictureData
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from Core.Accuaracy.ConfusionMatrix import ConfusionMatrix
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt



PATH_TO_TESTED_FOLDER = r'/home/antoneryomin/tensorflow/tensorflow/EXPERIMENTS_FOLDER/Downloaded'
#PATH_TO_TESTED_FOLDER = r'/media/antoneryomin/Новый том/!!!!___CHECKING_MODEL_QALITY___!!!!___NOT_TO_USE/botinki'


attribute_name = ["Krossovki", "Kedy", "Botinki i polubotinki", "Tufli", "Akvaobuv'", "Uggi", "Kazaki", "Galoshi",
                  "Unty", "Tapochki",  "Butsy", "Botforty", "Lofery", "Mokasiny", "Sapogi i polusapogi", "Bosonozhki",
                  "Slantsy", "Botil'ony", "Sandalii", "Baletki", "Slipony i espadril'i", "Dutiki i lunohody",
                  "Topsajdery", "Rezinovaja obuv'", "Botinki trekingovye", "Valenki"]
attribute_clasp = ["na shnurkah", "bez zastezhki", "na krjuchkah", "na knopkah", "na zamke", "na pugovitsah",
                   "karabin", "magnit", "na molnii", "na prjazhke", "na lipuchkah"]
attribute_gender = ["women", "men"]
attribute_type_of_heels = [" tanketka", " shpil'ka", "shpil'ka", "platforma", " standartnyj", " platforma", "tanketka",
                           "standartnyj", "bez kabluka"]
attribute_type_of_heels_platforma = ["platforma", " platforma"]
attribute_type_of_heels_kabluk = [" shpil'ka", "shpil'ka", " standartnyj", "standartnyj", "bez kabluka"]
transformed_dict = {'Slipony': "Slipony i espadril'i", "Botinki": "Botinki i polubotinki", "Rezinovye polusapogi": "Rezinovaja obuv'", "Rezinovye sapogi": "Rezinovaja obuv'", "Butsy zal'nye": "Butsy", "Shipovki": "Butsy",
                                "Sapogi": "Sapogi i polusapogi", "Polusapogi": "Sapogi i polusapogi", "Kedy na tanketke": "Kedy", "Lunohody": "Dutiki i lunohody", "Sabo": "Slantsy", "Espadril'i": "Slipony i espadril'i",
                                "Shlepantsy": "Slantsy"}


def accuracy_result_rate(catalog_dict, recognized_final_dict):
    """
    Метод для определения точности предсказанных классов.
    :param catalog_dict: словарь занчений полученный из каталога.
    :param recognized_final_dict: словарь значений полученный после распознавания изображения.
    :return: строку как соотношение
    """
    founded = 0
    for key in catalog_dict.keys():
        if "type_of_heels_" in key:
           if catalog_dict[key] == recognized_final_dict[key[0:-2]][int(key[-1])]:
               founded = founded + 1
        else:
            if catalog_dict[key] == recognized_final_dict[key][0]:
                founded = founded + 1
    return "{0}/{1}".format(founded, len(catalog_dict.keys()))


def average_value(dict_values):
    """
    Метод для подсчета среднего значения в словаре
    :param dict_values: словарь со значениями, где ключ это название класса, значение его "вероятность"
    :return: среднее значение "вероятности" в рамках этого класса
    """
    sum = 0
    for elem in dict_values:
        sum = sum + dict_values[elem]
    return sum / len(dict_values.keys())


def get_upper_average(dict_values, avg_value):
    """
    Возвращает только те значения, которые выше среднего
    :param dict_values: словарь из значений и их вероятностей
    :param avg_value: среднее значение в рамках данного класса
    :return: словарь, содержащий только пару ключ-значение, значение которой больше среднего
    """
    cleaned_dict = {}
    for elem in dict_values:
        if dict_values[elem] > avg_value:
            cleaned_dict[elem] = dict_values[elem]
    return cleaned_dict


def recognition_result(dict_marked, ordered_list):
    result_dict = {}
    for elem in dict_marked.keys():
        if dict_marked[elem] in ordered_list:
            pos_number = ordered_list.index(dict_marked[elem])
            result_dict[elem] = (dict_marked[elem], pos_number + 1)
        else:
            transformed_dict = {'Slipony': "Slipony i espadril'i", "Botinki": "Botinki i polubotinki",
                                "Rezinovye polusapogi": "Rezinovaja obuv'", "Rezinovye sapogi": "Rezinovaja obuv'",
                                "Butsy zal'nye": "Butsy", "Shipovki": "Butsy", "Sapogi": "Sapogi i polusapogi",
                                "Polusapogi": "Sapogi i polusapogi", "Kedy na tanketke": "Kedy",
                                "Lunohody": "Dutiki i lunohody", "Sabo": "Slantsy",
                                "Espadril'i": "Slipony i espadril'i", "Shlepantsy": "Slantsy"}
            if dict_marked[elem] in transformed_dict.keys():
                pos_number = ordered_list.index(transformed_dict[dict_marked[elem]])
                result_dict[elem] = (dict_marked[elem], pos_number + 1)
            else:
                print(dict_marked[elem])
    return result_dict


def mean_normilize(dict_values):
    avg = average_value(dict_values)
    updated_dict = {}
    for elem in dict_values.keys():
        updated_dict[elem] = dict_values[elem] / avg
    return updated_dict


def catalog_to_local_naming(dic):
    """
    Возвращает словарь, который прошел преобразование в контексте значений вектра выходных значений.
    :param dic: словарь значений, который получен из
    :return: словрь, в котором ключи изменены в нотации выходного вектора.
    """
    updated_dict = {}
    for key in dic.keys():
        if dic[key] in transformed_dict.keys():
            updated_dict[key] = transformed_dict[dic[key]]
        else:
            updated_dict[key] = dic[key]
    return updated_dict


def write_result(data):
    """
    Метод для записи данных в файл. Данные предсатавляют собой список списков, где на каждой из позиции в картеже
    определанные данные:
    1 позиция: итоговая вероятность (произведения всех вероятностей внутри)
    2 позиция: точность преfrom string import punctuationдсказания
    3 позиция: размеченный словарь
    4 позиция: распознанный словарь
    :param data: словарь из данных, описание выше
    :return: None
    """
    with open("result.csv", 'w') as f:
        writer = csv.writer(f, dialect='excel', lineterminator='\n')
        writer.writerows(data)


def sorted_list(list_results):
    """
    Метод для сортировки списка результатов по параметру ACCURACY_SCORE
    :param list_results:
    :return:
    """
    ordered_list_of_results = []
    min_element_list = []
    for elem in list_results:
        for sub_elem in elem.keys():
            for _ in elem[sub_elem]:
                accuracy_score = elem[sub_elem][_]['ACCURACY_SCORE']
                if len(min_element_list) == 0:
                    min_element_list.append(accuracy_score)
                    ordered_list_of_results.append(elem)
                else:
                    for i in range(0, len(min_element_list)):
                        if float(accuracy_score) <= float(min_element_list[i]):
                            min_element_list.insert(i, accuracy_score)
                            ordered_list_of_results.insert(i, elem)
                            break
                        elif float(accuracy_score) > float(min_element_list[-1]):
                            min_element_list.append(accuracy_score)
    return ordered_list_of_results


def parce_dict(dict_to_parce):
    for key_dict in dict_to_parce.keys():
        for key_pict_path in dict_to_parce[key_dict].keys():
            return (key_dict, key_pict_path, dict_to_parce[key_dict][key_pict_path]['ACCURACY_SCORE'],
                    dict_to_parce[key_dict][key_pict_path])


if __name__ == '__main__':
    label_lines = [line.rstrip() for line
                   in tf.gfile.GFile(r"/media/antoneryomin/Новый том/WORK/TENSORFLOW/MODELS/28122017/output_labels.txt")]
    with tf.gfile.FastGFile(r"/media/antoneryomin/Новый том/WORK/TENSORFLOW/MODELS/28122017/output_graph.pb", 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')
    final_result = []
    all_results = []
    list_to_analyze_accuracy = []
    for image_path in os.listdir(PATH_TO_TESTED_FOLDER):
        result_dict = {}
        marked_dict = {}
        recognized_dict = {}
        tested_picture = LamodaPicture(os.path.join(PATH_TO_TESTED_FOLDER, image_path))
        tested_pict_info = PictureInfo(tested_picture.path, 'catalogue', tested_picture.sku)
        picture_json = tested_pict_info.data_from_source

        #picture_json = ddfs.get_json(ddfs.get_url(tested_picture.get_pict_sku()))

        #print(picture_json_1)
        try:
            result_dict['pict_path'] = tested_pict_info.txt_info_path
            if tr.detect_language(picture_json['data']['name']) == 'ru':
                marked_dict['name'] = Result._recognized_value_to_normal(tr.translit(picture_json['data']['name'], reversed=True)).lower()
            else:
                marked_dict['name'] = Result._recognized_value_to_normal(picture_json['data']['name']).lower()


        # if tr.detect_language(picture_json['data'][0]['gender']) == 'ru:':
        #     marked_dict['gender'] = Results._recognized_value_to_normal(tr.translit(picture_json['data'][0]['gender'], reversed=True))
        # else:
        #     marked_dict['gender'] = Results._recognized_value_to_normal(picture_json['data'][0]['gender'])
        # #ДЕЛАЕМ ДОПУЩЕНИЕ, ЕСЛИ МЫ НЕ НАХОДИМ ПАРАМЕТР ТИП КАБЛУКА ТО МЫ ЕГО ПРОСТАВЛЯЕМ САМИ
        # if 'type_of_heels' in picture_json['data'][0]['attributes'].keys():
        #     if tr.detect_language(picture_json['data'][0]['attributes']['type_of_heels']) == 'ru':
        #         recogn_heels_splitted = tr.translit(picture_json['data'][0]['attributes']['type_of_heels'], reversed=True).split(',')
        #         for heel in recogn_heels_splitted:
        #             if heel in attribute_type_of_heels_platforma:
        #                 marked_dict['platforma'] = Results._recognized_value_to_normal(heel)
        #             elif heel in attribute_type_of_heels_kabluk:
        #                 marked_dict['kabluk'] = Results._recognized_value_to_normal(heel)
        # else:
        #     marked_dict['kabluk'] = "bez kabluka"
        #     #marked_dict['platforma'] = " platforma"
        #     marked_dict['platforma'] = "bez platformy"
        # if 'clasp' in picture_json['data'][0]['attributes'].keys():
        #     if tr.detect_language(picture_json['data'][0]['attributes']['clasp']) == 'ru':
        #         marked_dict['clasp'] = tr.translit(picture_json['data'][0]['attributes']['clasp'], reversed=True)
        #     else:
        #         marked_dict['clasp'] = picture_json['data'][0]['attributes']['clasp']
            result_dict['marked_data'] = marked_dict
            image_data = tf.gfile.FastGFile(os.path.join(PATH_TO_TESTED_FOLDER, image_path), 'rb').read()
            with tf.Session() as sess:
                softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
                predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})
                top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
                list_recognized_vector = []
                total_sum = 0
                count = 0
                dict_result = {}
                gender_dict = {}
                name_dict = {}
                clasp_dict = {}
                heels_platforma_dict = {}
                heels_kabluk_dict = {}
                for node_id in top_k:
                    human_string = label_lines[node_id].lower()
                    name_dict[human_string] = predictions[0][node_id].tolist()
                    if human_string in attribute_gender:
                        gender_dict[human_string] = predictions[0][node_id].tolist()
                    elif human_string in attribute_name:
                        name_dict[human_string] = predictions[0][node_id].tolist()
                    # elif human_string in attribute_clasp:
                    #     clasp_dict[human_string] = predictions[0][node_id].tolist()
                    # elif human_string in attribute_type_of_heels_platforma:
                    #     heels_platforma_dict[human_string] = predictions[0][node_id].tolist()
                    # elif human_string in attribute_type_of_heels_kabluk:
                    #     heels_kabluk_dict[human_string] = predictions[0][node_id].tolist()
                dict_result['name'] = name_dict
                # dict_result['clasp'] = clasp_dict
                # dict_result['gender'] = gender_dict
                # dict_result['platforma'] = heels_platforma_dict
                # dict_result['kabluk'] = heels_kabluk_dict
                result = Result(dict_result)
                normalized_dict = Result(result.dict_with_normilize()).result_to_ordered_dict()
                # dict_for_result = {tested_picture.get_pict_sku(): {tested_pict_info.txt_info_path: {'CATALOG_DICTIONARY': marked_dict,
                #                                                               'RECOGNIZED_DICT': Results.get_only_winner(normalized_dict),
                #                                                               'ACCURACY_SCORE': Results.accuracy_score(Results.compare_with_marked_dict(recognized_dict=normalized_dict, marked_dict=marked_dict))}}}
                dict_for_result = {tested_pict_info.txt_info_path: {tested_picture.get_pict_sku(): {'CATALOG_DICTIONARY': marked_dict,
                                                                              'RECOGNIZED_DICT': Result.get_only_winner(normalized_dict),
                                                                              'ACCURACY_SCORE': Result.accuracy_score(Result.compare_with_marked_dict(recognized_dict=normalized_dict, marked_dict=marked_dict))}}}
                final_result.append(dict_for_result)
        except TypeError:
            print(image_path)

    #         list_to_analyze_accuracy.append((Results.accuracy_score(Results.compare_with_marked_dict(
    #             recognized_dict=normalized_dict, marked_dict=marked_dict)),
    #             Results.compare_with_marked_dict_without_prob(Results.get_only_winner(normalized_dict, False),
    #                                                          marked_dict)))
    for dict_result in final_result:
        Result.write_results_to_json(r'/media/antoneryomin/Новый том/WORK/TENSORFLOW/MODELS/28122017/benchmarks.json', dict_result)
    # conf_matrix = ConfusionMatrix(r'/media/antoneryomin/Новый том/WORK/TENSORFLOW/MODELS/16112017/result.json').data_to_confusion_library('name')
    # classes = ConfusionMatrix.get_classes(conf_matrix)
    # plt.figure()
    # ConfusionMatrix.plot_confusion_matrix(confusion_matrix(conf_matrix[0], conf_matrix[1]), classes)
    # plt.show()
    # updated_list = []
    # for elem in sorted_list(final_result):
    #     updated_list.append(parce_dict(elem))
    # with open('final_results.txt', 'w') as f:
    #     count = 0
    #     for element in updated_list:
    #         count = count + 1
    #         f.write("PICT_PATH: {0} RECOGNIZED_VALUE: {1}".format(element[1], element[3]) + '\n')