import os
import json
from levinshtein_distance import Similarity
from Core.Accuaracy.ConfusionMatrix import ConfusionMatrix


class ResultsFromFile:
    def __init__(self, path_to_file):
        self.path_to_file = path_to_file


    @property
    def path_to_file(self):
        return self._path

    @path_to_file.setter
    def path_to_file(self, path):
        if not path: raise Exception("Path to results file doesn't exist.")
        elif not os.path.exists(path): raise FileNotFoundError
        self._path = path

    def _read_file(self):
        with open(self.path_to_file, 'r') as f:
            return json.load(f)

    def get_all_sku(self):
        return [key for key in self._read_file().keys()]

    def _get_result_tree(self):
        #TODO Убрать в методе хардкод на 1 тип аттрибута.
        final_dict = {}
        all_data = self._read_file()
        for sku in all_data:
            final_dict[sku] = []
            for file_path in all_data[sku].keys():
                recognized_data = [all_data[sku][file_path]['CATALOG_DICTIONARY']['name'], all_data[sku][file_path]['RECOGNIZED_DICT']['name'][0]]
                final_dict[sku].append({file_path: recognized_data})
        return final_dict

    def get_normilized_data(self):
        """
        Метод для анализа выходного словаря значений. Словарь делится на 2 категории, те где мы считаем, что данные
        из каталога подозрительные, те они слишком сильно отличаются от наших базовых значений, вторая категория,
        содержит
        :return:
        """
        normalized_results = {'normal': [], 'strange': []}
        similarity = Similarity()
        analyzed_dict = self._get_result_tree()
        for sku in analyzed_dict.keys():
            for pict_recogn_results in analyzed_dict[sku]:
                for pict_name in pict_recogn_results.keys():
                    results = pict_recogn_results[pict_name]
                    if similarity.returned_word(results[0]) in similarity.basic_words_list:
                        normalized_results["normal"].append({sku: {pict_name: [similarity.returned_word(results[0]), results[1]]}})
                    else:
                        normalized_results["strange"].append({sku: {pict_name: results}})
        return normalized_results

    def get_only_result_pares(self):
        """
        метод который вовзравщает лишь пары значений, данных из каталога и тех что мы распознали.
        :return: словарь
        """
        results_dict = self.get_normilized_data()
        dict_to_recognize = {'strange': [], 'normal': []}
        for sku_result in results_dict['normal']:
            for sku in sku_result.keys():
                for file_name in sku_result[sku]:
                    dict_to_recognize['normal'].append(sku_result[sku][file_name])
        for sku_result in results_dict['strange']:
            for sku in sku_result.keys():
                for file_name in sku_result[sku]:
                    dict_to_recognize['strange'].append(sku_result[sku][file_name])
        return dict_to_recognize

    def pares_to_dict(self, type_name, classes):
        results_dict = {}
        for class_name in classes:
            for pict_result in self.get_normilized_data()[type_name]:
                for class_data in pict_result.keys():
                    for sku in pict_result[class_data]:
                        for pare in pict_result[class_data][sku]:
                            if class_name == pare[0] and class_name == pare[1]:
                                if 'diagonal' in results_dict[class_name].keys():
                                    results_dict[class_name]['diagonal'] = results_dict[class_name]['diagonal'] + 1
                                else:
                                    results_dict[class_name] = {'diagonal': 1}
                            elif class_name == pare[0] and class_name != pare[1]:
                                if tuple(pare) in results_dict[class_name].keys():
                                    results_dict[class_name][tuple(pare)] = results_dict[class_name][tuple(pare)] + 1
                                else:
                                    results_dict[class_name][tuple(pare)] = 1
        return results_dict

    @staticmethod
    def count_class_numbers_value(pares):
        final_dict = {}
        catalog_classes = {}
        predicted_classes = {}
        for pare in pares:
            if pare[0] in catalog_classes.keys():
                catalog_classes[pare[0]] = catalog_classes[pare[0]] + 1
            else:
                catalog_classes[pare[0]] = 1
            if pare[1] in predicted_classes.keys():
                predicted_classes[pare[1]] = predicted_classes[pare[1]] + 1
            else:
                predicted_classes[pare[1]] = 1
        final_dict['catalog'] = catalog_classes
        final_dict['predicted'] = predicted_classes
        return final_dict

    @staticmethod
    def get_class_names(final_dict):
        labels = []
        for key in final_dict.keys():
            for subvalue in final_dict[key]:
                if subvalue not in labels:
                    labels.append(subvalue)
        return labels