"""
Class for SKU description
"""
import json
import logging
import os
import time
import urllib.request as urllib
import requests
from Core.Configuration.properties import APIGW_SERVER, PICTURE_SERVER


class SKU:
    def __init__(self, id, attributes={}, pict_pathes=[], label_pathes=[]):
        self.id = id
        self.pict_pathes = pict_pathes
        self.attributes = attributes
        self.label_pathes = label_pathes

    def __str__(self):
        return str(self.id)

    def set_sku_json(self):
        """
        Getting SKU information from APIGW server.
        :return:
        """
        try:
            r = requests.post(str(APIGW_SERVER), data=json.dumps({'product_sku': self.__str__()}))
            self.attributes = r.json()
        except requests.exceptions.RequestException as e:
            print(e)

    def get_pictures_url(self):
        """
        Method for getting picture URLs.
        :return:
        """
        self.set_sku_json()
        try:
            temp = self.attributes
            print(temp)
            temporary_list = temp['product']['gallery']
            list_pictures = []
            for i in temporary_list:
                list_pictures.append(PICTURE_SERVER + i)
            logging.info(list_pictures)
            return list_pictures
        except:
            ConnectionError
            logging.error("Can't take URL for SKU: {0}".format(self.id))
            return self.id

    def get_list_sku_atrributes(self):
        """
        Getting all unique attributes from sku
        :return: list of all SKU attributes
        """
        list_attr = []
        for key in self.attributes:
            if key not in list_attr:
                list_attr.append(key)
        return list_attr

    def _download_pictures(self, main_folder, time_sleep):
        """
        DEPRICATED METHOD SOON WILL BE DELETED.
        Download all pictures fro current SKU.
        :param main_folder: path to the folder where all pictures for this SKU will be stored
        :param time_sleep: latence between two requests
        :return:
        """
        list_urls = self.get_pictures_url()
        if len(list_urls) > 0:
            counter = len(list_urls)
            for url in list_urls:
                try:
                    path = os.path.join(main_folder, self.skuid) + 'pic{0}.jpg'.format(counter)
                    urllib.urlretrieve(url, path)
                    logging.debug("Pictures had been downloaded from {0} and saved in: {1}".format(url, path))
                    time.sleep(time_sleep)
                    counter = counter - 1
                except:
                    logging.error("Pictures from URL: {0} can't be donwloaded".format(url))
        else:
            logging.info("Can't take urls for SKU: {0}".format(self.skuid))

    def download_pictures(self, main_folder, time_sleep):
        """
        Download all pictures fro current SKU.
        :param main_folder: path to the folder where all pictures for this SKU will be stored
        :param time_sleep: latency between two requests
        :return:
        """
        list_urls = self.get_pictures_url()
        if len(list_urls) > 0:
            counter = len(list_urls)
            for url in list_urls:
                try:
                    path = os.path.join(main_folder, self.id) + '_pic{0}.jpg'.format(counter)
                    u = urllib.urlopen(url)
                    site_length = urllib.urlopen(url).info()['Content-Length']
                    urllib.urlretrieve(url, path)
                    if int(u.info()['Content-Length']) != os.stat(path).st_size:
                        attemp = 0
                        while attemp <= 3:
                            urllib.urlretrieve(url, path)
                            if site_length == os.stat(path).st_size:
                                break
                            else:
                                attemp += 1
                        logging.error("Pictures from URL: {0} can't be downloaded".format(url))
                    time.sleep(time_sleep)
                    counter = counter - 1
                except:
                    logging.error("Pictures from URL: {0} can't be donwloaded".format(url))

    def get_attribute_value(self, attributeName):
        """
        Method for getting all attribute values for this SKU.
        :param attributeName: attribute name which should be in SKU object.
        :return: attribute values list if SKU object has attribute name, otherwise return None.
        """
        try:
            return self.attributes[attributeName]
        except:
            logging.debug("Sku {0} hasn't attribute {1}".format(self.skuid, attributeName))
            return None

    # ----------------------------------------------------------------------------------------------------------------------
    # working with SKUs file
    def get_paths_to_sku(self, root_directory):
        """
        Method for getting all pathes from folder where already pictures downloaded.
        :param root_directory: directory with all downloaded pictures. 
        :return: list with paths to files if files exists otherwise none. 
        """
        files = os.listdir(root_directory)
