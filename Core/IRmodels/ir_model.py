import os


class IRmodel:
    def __init__(self, path_to_pb, path_to_txt):
        self.path_to_pb = path_to_pb
        self.path_to_txt = path_to_txt

    @property
    def path_to_pb(self):
        return self._path_to_pb

    @path_to_pb.setter
    def path_to_pb(self, p_b):
        if not os.path.exists(p_b):
            raise FileNotFoundError
        self._path_to_pb = p_b

    @property
    def path_to_txt(self):
        return self._path_to_txt

    @path_to_txt.setter
    def path_to_txt(self, p_t):
        if not os.path.exists(p_t):
            raise FileNotFoundError
        self._path_to_txt = p_t

    def get_available_classes(self):
        classes = []
        with open(self.path_to_txt, 'r') as f:
            for line in f.readlines():
                classes.append(line.strip())
        return classes

    def get_model_version(self):
        return self.path_to_txt.split(r'/')[-2]
