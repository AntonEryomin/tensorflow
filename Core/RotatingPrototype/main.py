"""
Module for rotating pictures
"""
from PIL import Image
import os
import shutil
from Core.RotatingPrototype.window import Window
import Core.Configuration.properties as prop

PATH_TO_FOLDER_WITH_PICTURES = '/media/tensorflow/Store/Selfies'
PATH_TO_FOLDER_WHERE_TO_SAVE = '/media/tensorflow/Store/SelfiesWindow'


def pic_transformation(pic_path):
    rotating_pool = [90, 180, 270]
    original_image = Image.open(pic_path)
    shutil.copyfile(pic_path, os.path.join(PATH_TO_FOLDER_WHERE_TO_SAVE, os.path.basename(pic_path)))
    for rotation in rotating_pool:
        temp_pic = original_image.rotate(rotation)
        temp_pic.save(os.path.join(
            os.path.join(PATH_TO_FOLDER_WHERE_TO_SAVE, pic_path.split('.jpg')[0] + '_' + str(rotation) + '.jpg')))


picture_paths = []
for pic in os.listdir(PATH_TO_FOLDER_WITH_PICTURES):
    if pic != '.DS_Store':
        pic_path = os.path.join(PATH_TO_FOLDER_WITH_PICTURES, pic)
        width, height = Image.open(pic_path).size
        window = Window(max_width=width, max_height=height, particularity=int(prop.LOW))
        a = window.coordinates_calculate()
        img = Image.open(pic_path)
        number = 0
        for coordinate in a:
            temp = img.crop((coordinate[0], coordinate[1], coordinate[2], coordinate[3]))
            print(pic_path.split('/')[-1].split('.jpg')[0] + '_' + str(number) + '.jpg')
            print(PATH_TO_FOLDER_WHERE_TO_SAVE,
                                   pic_path.split('.jpg')[0] + '_' + str(number) + '.jpg')
            temp.save(os.path.join(PATH_TO_FOLDER_WHERE_TO_SAVE,
                      pic_path.split('/')[-1].split('.jpg')[0] + '_' + str(number) + '.jpg'))
            number = number + 1