from fractions import Fraction


class Window:
    def __init__(self, max_width, max_height, particularity):
        self.window_width = max_width // particularity
        self.step_width = max_width // particularity ** 2
        self.maxWidth = max_width
        self.maxHeight = max_height
        self.rate = Fraction(max_width, max_height)
        self.window_height = self.window_width // self.rate
        self.step_height = self.step_width // self.rate


    def coordinates_calculate(self):
        """
        Method for windows coordinates counting.
        :return: list of tuples with coordinates 
        """
        start_coordinate = [0, 0, self.window_width, self.window_height]
        coordinate_list = [start_coordinate]
        for height in range(0, (
            (self.maxHeight - self.window_height) // self.step_height) * self.step_height + self.window_height - self.step_height,
                            self.step_height):
            new_a_point = [0, height]
            new_b_point = [self.window_width, height + self.window_height]
            coordinate_list.append([new_a_point[0], new_a_point[1], new_b_point[0], new_b_point[1]])
            for width in range(0, (
                (self.maxWidth - self.window_width) // self.step_width) * self.step_width + self.window_width - self.step_width,
                               self.step_width):
                new_a_point = [new_a_point[0]+self.step_width, new_a_point[1]]
                new_b_point = [new_b_point[0]+self.step_width, new_b_point[1]]
                square = [new_a_point[0], new_a_point[1], new_b_point[0], new_b_point[1]]
                coordinate_list.append(square)
        return coordinate_list
