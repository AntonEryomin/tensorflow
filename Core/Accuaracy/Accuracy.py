import os


class Accuracy:
    def __init__(self, path_to_csv_file):
        self.csv_path = path_to_csv_file
        self.right_answer = (os.path.splitext(path_to_csv_file)[0]).split('_')[1]

    def if_result_true(self, answer):
        if answer.lower() == self.right_answer.lower():
            return True
        else:
            return False

    def count_accuracy(self):
        number_of_right_answers = 0
        number_of_wrong_answers = 0
        list_of_false_pict = []
        lines_number = 0
        with open(self.csv_path, 'r') as f:
            for result_string in f.read().splitlines():
                if result_string != '':
                    result_dict = Accuracy.parse_results_string(result_string)
                    if self.if_result_true(Accuracy.parse_results_string(result_string)['result recognition']) == True:
                        number_of_right_answers = number_of_right_answers + 1
                    else:
                        number_of_wrong_answers = number_of_wrong_answers + 1
                        list_of_false_pict.append(result_dict['picture'])
                    lines_number = lines_number + 1
        return {'model_name': self.right_answer, 'accuracy': (number_of_right_answers / lines_number), 'list_failed_recognized_pictures': list_of_false_pict, 'number_of_wrong_answers': number_of_wrong_answers, 'number_of_right_answers': number_of_right_answers, 'total_answers': lines_number
                }

    @staticmethod
    def parse_results_string(string_on_parsing):
        """
        Parse string on elements like: picture name, main_attribute, result_recognition, probability
        :return: dictionary with parsed string
        """
        list_results = string_on_parsing.split(',')
        return {'picture': list_results[0], 'model name': list_results[1], 'result recognition': list_results[2],
                'probability': list_results[3]}