# -*- coding: utf-8 -*-
"""
Kласс для создания матрицы ошибок. Для создания объекта данного класса необходимо иметь json файл,
в котором хранились бы результаты распознавания картинки.
"""

import itertools
import matplotlib.pyplot as plt
import numpy as np
from levinshtein_distance import Similarity


class ConfusionMatrix:
    def __init__(self, data_for_plot):
        self.data_for_plot = data_for_plot

    def get_dict_predictions(self):
        """
        Метод для получения словаря предсказаний результатов.
        :return: Словарь, в котором первое значение это уникальный аттрибут, ключами для данного словаря являются
        значения уникальных , а ключами в словаре является список из кортежей, где первый элемент в кортеже
        это значение из каталога (expected/true значение), а второе значение это значение которое выдает модель.
        """
        similarity = Similarity()
        json_obj = self.data_for_plot
        predicted_dict = {}
        for key, value in json_obj.items():
            for value_key, value_val in value.items():
                for key in value_val['CATALOG_DICTIONARY'].keys():
                    if key in predicted_dict.keys():
                        predicted_dict[key].append(
                            (similarity.returned_word(value_val['CATALOG_DICTIONARY'][key]), value_val['RECOGNIZED_DICT'][key][0]))
                    else:
                        predicted_dict[key] = [
                            (similarity.returned_word(value_val['CATALOG_DICTIONARY'][key]), value_val['RECOGNIZED_DICT'][key][0])]
        return predicted_dict

    def data_to_confusion_library(self):
        """
        Метод для подготовки данных для использования в блиблиотеки confusion matrix.
        :return: список списков данных для подготовки данных.
        """
        y_true = []
        y_pred = []
        for elem in self.data_for_plot:
            y_true.append(elem[0])
            y_pred.append(elem[1])
        return (y_true, y_pred)

    @staticmethod
    def get_classes(data_confusion_matrix):
        classes = []
        for _ in data_confusion_matrix:
            for cls in _:
                if cls not in classes:
                    classes.append(cls)
        return classes

    @staticmethod
    def plot_confusion_matrix(cm, classes,
                              normalize=False,
                              title='Confusion matrix',
                              cmap=plt.cm.Blues):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
            print("Normalized confusion matrix")
        else:
            print('Confusion matrix, without normalization')
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=90)
        plt.yticks(tick_marks, classes)
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')