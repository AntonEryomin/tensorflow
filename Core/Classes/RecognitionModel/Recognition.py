import os
from pathlib import Path
import tensorflow as tf
import Core.Classes.DataManipulation.datafromfiles as dff
import Core.Classes.DataManipulation.resultsoutput as res
from Core.Configuration.properties import PATH_TO_FOLDER_WITH_ALL_MODELS
# Turn off tensorflow debug information
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


class Recognition:
    """
    Class for working with recognition process.
    """
    def __init__(self, path_to_picture_file, attribute_name):
        """
        :param path_to_picture_file: path to recognized picture
        :param attribute_name: path to attribute name
        """
        self.path_to_picture_file = path_to_picture_file
        self.attribute_name = attribute_name

        def get_list_subdir(current_dir_path):
            current_dir = Path(current_dir_path)
            if attribute_name == current_dir.name:
                return (os.path.join(current_dir_path, self.attribute_name + '_graph.pb'), os.path.join(current_dir_path, self.attribute_name + '_labels.txt'))
            else:
                for subdir in current_dir.iterdir():
                    if subdir.is_dir():
                        result = get_list_subdir(os.path.join(current_dir_path, subdir.name))
                        if result is not None:
                            return result

        attr_files = get_list_subdir(PATH_TO_FOLDER_WITH_ALL_MODELS)
        if len(attr_files) > 0:
            self.path_to_model = attr_files[0]
            self.path_to_list_attributes = attr_files[1]

    def get_list_attributes(self):
        list_attributes = []
        attributes_list_path = self.path_to_model[:-8] + 'labels.txt'
        with open(attributes_list_path, 'r') as file:
            for line in file:
                list_attributes.append(line.splitlines()[0])
        return list_attributes

    def recognition_run(self):
        """
        Method for start recognition process
        :return: dictionary with calculation results 
        """
        result_collection = {}
        image_data = tf.gfile.FastGFile(self.path_to_picture_file, 'rb').read()
        result_collection[os.path.basename(self.path_to_picture_file)] = []
        result_collection = res.save_models(self.path_to_picture_file, self.attribute_name, result_collection)
        sub_attributes = self.get_list_attributes()
        with tf.gfile.FastGFile(self.path_to_model, 'rb') as f:
            tf.reset_default_graph()
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            _ = tf.import_graph_def(graph_def, name='')
        with tf.Session() as sess:
            # Feed the image_data as input to the graph and get first prediction
            softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
            predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})
            # Sort to show labels of first prediction in order of confidence
            top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
            attr_value = []
            for node_id in top_k:
                tempDict = {}
                human_string = sub_attributes[node_id]
                score = predictions[0][node_id]
                tempDict[human_string] = round(score.item(), 3)
                attr_value.append(tempDict)
            final_results = res.threshould_output(attr_value)
            res.add_volume_res(dff.revert_dictionary(final_results), self.path_to_picture_file, self.attribute_name,
                               result_collection)
        return result_collection

    @staticmethod
    def get_list_all_available_models():
        return os.listdir(PATH_TO_FOLDER_WITH_ALL_MODELS)