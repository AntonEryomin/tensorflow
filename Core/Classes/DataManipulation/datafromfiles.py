import os
import re
import shutil
import sys
import logging
import ast
import io
from PIL import Image
import transliterate as tr
from Core.Configuration import properties as prop


# -----------------------------------------------------------------------------------------------------------------------
# Methods for working with folders
# -----------------------------------------------------------------------------------------------------------------------
def create_folders(root_folder_path, list_folder_names):
    """
    Method for creating folders.
    :param root_folder_path: path to root folders
    :param list_folder_names: list of folders Name
    :return: none
    """
    for folder in list_folder_names:
        path = root_folder_path + str(folder)
        if not os.path.exists(path):
            os.makedirs(path)
            logging.info("Folders {0} have been successfully created.".format(list_folder_names))


def get_all_attributes_from_folder(path_dir):
    """
    Method for getting all attributes from folder.
    :param path_dir: path to the directory which will be scanned
    :return: list attribute names
    """
    list_attributes = []
    all_files = os.listdir(path_dir)
    for attr in all_files:
        if attr != ".DS_Store":
            list_attributes.append(attr[0:-4])
    return list_attributes


# ----------------------------------------------------------------------------------------------------------------------
# Other methods
# ----------------------------------------------------------------------------------------------------------------------
# Create files for working with data
def create_file_descrition(path, category_name):
    file = open(path + '/{0}.txt'.format(category_name), 'w')
    file.writelines("Category: " + category_name + "\n")
    file.close()


def delete_odd_elements(elements_list):
    """
    Method for deleting odd elements.
    :param elements_list: list which should be checked
    :return: updated list.
    """
    updated_list = []
    for elem in elements_list:
        if isinstance(elem, type(None)) or elem != '':
            updated_list.append(elem)
    return updated_list


# ----------------------------------------------------------------------------------------------------------------------
# Methods for copying list of files from one directory to another
# ----------------------------------------------------------------------------------------------------------------------
def copy_files(list_start, directory):
    """
    Method for copying list of files from one directory to final directory.
    :param list_start: list of paths to files which need to be copy
    :param directory: directory where files will be copied
    :return: none
    """
    if isinstance(list_start, list) and len(list_start) != 0:
        for filePath in list_start:
            shutil.copy(filePath, r'{0}'.format(directory))


def copy_file(source_path, desitnation_path):
    shutil.copy(r'{0}'.format(source_path), r'{0}'.format(desitnation_path))


# ----------------------------------------------------------------------------------------------------------------------
# Print iterations progress
def print_progress(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        barLength   - Optional  : character length of bar (Int)
    """
    format_str = "{0:." + str(decimals) + "f}"
    percent = format_str.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '█' * filled_length + '-' * (bar_length - filled_length)
    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percent, '%', suffix)),
    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()


# ----------------------------------------------------------------------------------------------------------------------
def download_sku_list(sku_obj_list):
    i = 0
    l = len(sku_obj_list)
    print_progress(i, l, prefix='Progress:', suffix='Complete', bar_length=50)
    for sku in sku_obj_list:
        sku.download_pictures(os.path.join(prop.PATH_TO_PICTURES_FOLDER, prop.LIST_PICTURES_FOLDERS[0]),
                              prop.TIMEOUT_REQUESTS)
        i += 1
        print_progress(i, l, prefix='Progress:', suffix='Complete', bar_length=50)


def get_path_to_pictures(path_folder):
    """
    Method for getting paths to all pictures in folder
    :param path_folder: path to folder which will be scanned
    :return: list with paths to pictures
    """
    pict_path_list = []
    print(path_folder)
    for pict in os.listdir(path_folder):
        if pict != '.DS_Store' and pict[-3:] == 'jpg':
            pict_path_list.append(os.path.join(prop.RECOGNITION_PICTURES_FOLDER, pict))
    return pict_path_list


def get_path_to_categ_files():
    """
    :return:
    """
    path_to_root_folder = os.path.join(prop.PATH_TO_PICTURES_FOLDER, prop.LIST_PICTURES_FOLDERS[1])
    dict_categ_files = {}
    for folder in os.listdir(path_to_root_folder):
        dict_categ_files[folder] = []
        for file in os.listdir(os.path.join(path_to_root_folder, folder)):
            if file.endswith(".txt"):
                dict_categ_files[folder].insert(0, os.path.join(path_to_root_folder, folder, file))
            if file.endswith(".pb"):
                dict_categ_files[folder].insert(1, os.path.join(path_to_root_folder, folder, file))
    return dict_categ_files


def get_name_categ_files(categ):
    path_to_root_folder = os.path.join(prop.PATH_TO_PICTURES_FOLDER, prop.LIST_PICTURES_FOLDERS[1])
    for file in os.listdir(path_to_root_folder + categ):
        if file.endswith(".pb"):
            return file


# ----------------------------------------------------------------------------------------------------------------------
# Translation process.
# ----------------------------------------------------------------------------------------------------------------------
def get_transliterate(text):
    """
    Method for getting dictioanary where key is original word, value is translated word.
    :param text: text - word that need to be translated
    :return: dictionary {original word: translated word}
    """
    translated_text = re.sub("[',&,-]", '', text)
    translated_text = re.sub("[ ]", '', translated_text)
    if tr.detect_language(translated_text) == 'ru':
        return {text: tr.translit(translated_text, reversed=True)}
    else:
        return {text: translated_text}


def get_translated_attr_from_dict(dictionary, category, original_attr):
    list_dict = dictionary[category]
    for attr_fict in list_dict:
        if original_attr() in attr_fict.keys():
            return attr_fict[original_attr]


def get_translated_dict_from_file(path_to_file):
    """
    Method for getting dictionary with pares original word : translated meaning of this word.
    :param path_to_file: path to file where store translation meanings
    :return: list of dictionaries
    """
    list_dict = []
    with io.open(path_to_file, 'r', encoding='utf-8') as file:
        for line in file:
            list_dict.append(ast.literal_eval(line.rstrip('\n')))
    return list_dict


def write_translated_dict_to_file(file_path, list_of_dict):
    """
    Method for writing dictionaries to file. Additional dictionaries will be writing only if they are new one.
    :param file_path: path to file where dictionaries are stored.
    :param list_of_dict: dictionary which planned to store in file
    :return: none
    """
    if os.path.isfile(file_path):
        current_list = get_translated_dict_from_file(file_path)
        for elem in list_of_dict:
            elem_key = list(elem.keys())[0]
            if elem_key not in [list(key.keys())[0] for key in current_list]:
                with io.open(file_path, 'a', encoding='utf-8') as file:
                    file.write('{0}\n'.format(elem))
    else:
        with io.open(file_path, 'a', encoding='utf-8') as file:
            for elem in list_of_dict:
                file.write('{0}\n'.format(elem))


def get_key_by_value(file_path, value):
    dictionary = get_translated_dict_from_file(file_path)
    for elem in dictionary:
        if value == list(elem.values())[0]:
            return str(list(elem.keys())[0])
    return value


def revert_dictionary(non_translated_dict):
    output_list = []
    for elem in non_translated_dict:
        for key in elem:
            output_list.append({get_key_by_value(os.path.join(prop.INPUT_FILES_FOLDER, prop.FILE_TRANSLATIONS), key): elem[key]})
    return output_list


def convert_to_jpg(path_to_image):
    img = Image.open(path_to_image)
    img.save(path_to_image, 'JPEG')