#TODO НА РЕФАКТОРИНГ
import csv
import os
import numpy as np
import io
import Core.Classes.Model.skuServices as skuserv
from Core.Configuration import properties as prop


def save_models(path_to_pic, model_name, dic):
    """
    :param path_to_pic: 
    :param model_name: 
    :param dic: 
    :return: 
    """
    if len(dic[os.path.basename(path_to_pic)]) == 0:
        dic[os.path.basename(path_to_pic)].append({model_name: []})
        return dic
    for elem in dic[os.path.basename(path_to_pic)]:
        if model_name not in elem.keys():
            dic[os.path.basename(path_to_pic)].append({model_name: []})
            return dic


def add_volume_res(result_dict_list, path_to_pic, model_name, dic):
    """
    
    :param result_dict_list: 
    :param path_to_pic: 
    :param model_name: 
    :param dic: 
    :return: 
    """
    for model in dic[os.path.basename(path_to_pic)]:
        if model_name in model.keys():
            for result in result_dict_list:
                model[model_name].append(result)


def get_category_list_from_file(path):
    """
    Method for getting categories list from file.
    :param path: path to file with categories.
    :return: list with categories.
    """
    category_list = []
    with io.open(path, encoding='utf-8') as f:
        category_f = f.read().splitlines()
        for category in iter(category_f):
            category_list.append(category)
    return category_list


# ----------------------------------------------------------------------------------------------------------------------
# Get data from output dictionary
# ----------------------------------------------------------------------------------------------------------------------
def get_pictures_list(dict_results):
    """
    Method for getting all pictures from result Dictionary as list.
    :param dict_results: dictionary with all calculated data. 
    :return: list pictures which were recognized. 
    """
    list_pictures = []
    for key in dict_results.keys():
        list_pictures.append(key)
    return list_pictures


def get_list_uniques_pict_names(list_pict_names):
    """
    Method for getting list unique pictures names from list.
    :param list_pict_names: list with different pictures name, elements of this list looks like 'SKUpic3.jpg' 
    :return: 
    """
    list_unique_pic = []
    for pic in list_pict_names:
        current_pic = skuserv.get_sku_from_picture_name(pic)
        if current_pic not in list_unique_pic:
            list_unique_pic.append(current_pic)
    return list_unique_pic


def get_sku_and_pictures_dict(list_sku, list_pictures_name):
    """
    :param list_sku: 
    :param list_pictures_name: 
    :return: 
    """
    dict_sku_pictures = {}
    for sku in list_sku:
        temp_list = []
        for picName in list_pictures_name:
            if sku in picName:
                temp_list.append(picName)
        if len(temp_list) > 0:
            dict_sku_pictures[sku] = temp_list
        else:
            dict_sku_pictures[sku] = ''
    return dict_sku_pictures


def get_pictures_attr_dict(pic_name, dict_results):
    """
    Method for getting list results for picture.
    :param pic_name: 
    :param dict_results: dictionary with all results.
    :return: List attributes for current picture with results. 
    """
    return dict_results[pic_name]


def get_unique_attribute_names(list_result_for_pictures):
    """
    Method for getting all unique attribute names in list
    :param list_result_for_pictures: list with all calculated results for pictures for one sku. 
    :return: list unique attributes name for this sku. 
    """
    list_unique_attr = []
    for results in list_result_for_pictures:
        for result in results:
            for key in result.keys():
                if key not in list_unique_attr:
                    list_unique_attr.append(key)
    return list_unique_attr


def get_category_results_list(category_name, list_results):
    """
    Method for getting list results for current category name.
    :param category_name: category name
    :param list_results: dictionary with all categories for picture.
    :return: List with results for current category. 
    """
    for categ in list_results:
        if category_name in categ.keys():
            return categ[category_name]
    return "Can't find category: {0} in list".format(category_name)


def get_lists_results_value(dict_results):
    """ 
    :param dict_results: 
    :return: 
    """
    output_lists = []
    for pic in dict_results.keys():
        for attr in dict_results[pic]:
            for key in attr.keys():
                output_lists.append(attr[key])
    return output_lists


def calculate_avg_value(list_pares):
    """
    Method for calculating average value and return dict with max value for attr_mean.
    :param list_pares: list with dictionaries, where dict is attr_mean and value 
    :return: dictionary where value is average meaning and max from list. 
    """
    unique_attr_meaning = []
    for pare in list_pares:
        for attrMeaning in pare.keys():
            if attrMeaning not in unique_attr_meaning:
                unique_attr_meaning.append(attrMeaning)
    list_dict = []
    for attr_meaning in unique_attr_meaning:
        counter = 0
        volume = 0
        temp_dict = {}
        for pare in list_pares:
            if attr_meaning in pare.keys():
                volume = volume + pare[attr_meaning]
                counter = counter + 1
        temp_dict[attr_meaning] = float(volume / counter)
        list_dict.append(temp_dict)
    if len(list_dict) == 0:
        return list_dict[0]
    else:
        result_dict = {}
        for pare in list_dict:
            for key in pare.keys():
                result_dict[key] = pare[key]
        max_value = max(result_dict, key=lambda i: result_dict[i])
        final_dict = {}
        final_dict[max_value] = result_dict[max_value]
        return final_dict


def threshold_output(list_results):
    """
    Method implements THRESHOLD LOGIC. To find out more, look at the file READEME.md.
    :param list_results: type:list; every element of this list should be dictionary 
    :return: updated list with Threshold logic. 
    """
    if prop.THRESHOLD_BOOLEAN == False:
        return list_results
    output_dict = []
    if prop.THRESHOLD_BOOLEAN == True:
        for pare in list_results:
            for attrName in pare.keys():
                if pare[attrName] > float(prop.THRESHOLD_VALUE):
                    output_dict.append({attrName: pare[attrName]})
        return output_dict


def avg_mean(list_pictures_data_result):
    """
    Method for calculating average meaning of probability for all pictures.
    :param list_pictures_data_result: dictionaries list with all data after recognition. 
    :return: calculated dictionary.
    """
    sku_pict_dict = get_sku_and_pictures_dict(
        list_sku=get_list_uniques_pict_names(get_pictures_list(list_pictures_data_result)),
        list_pictures_name=get_pictures_list(list_pictures_data_result))
    final_dictionary = {}
    for sku in sku_pict_dict.keys():
        final_dictionary[sku] = {}
        dict_sku_results = {}
        dict_sku_results[sku] = []
        for picName in sku_pict_dict[sku]:
            dict_sku_results[sku].append(get_pictures_attr_dict(pic_name=picName, dict_results=list_pictures_data_result))
        unique_attribute_name = get_unique_attribute_names(dict_sku_results[sku])
        for attrName in unique_attribute_name:
            final_dictionary[sku][attrName] = {}
            all_values = []
            for attributes in dict_sku_results[sku]:
                for attribute in attributes:
                    for attrValues in attribute[attrName]:
                        all_values.append(attrValues)
            final_dictionary[sku][attrName] = calculate_avg_value(all_values)
    return final_dictionary


# ----------------------------------------------------------------------------------------------------------------------
# Save data to file
# ----------------------------------------------------------------------------------------------------------------------
def save_results_to_txt_file(data_dict):
    """
    
    :param data_dict: 
    :return: 
    """
    results_file = open(prop.RECOGNITION_PICTURES_FOLDER + 'Results.txt', 'a')
    for key in data_dict.keys():
        results_file.write('----------------------------------------\n')
        results_file.write(key + '\n')
        category_list = data_dict[key]
        for category_result in category_list:
            category_name = [categ for categ in category_result.keys()]
            results_file.write("    ->{0}\n".format(category_name[0]))
            list_results = category_result.get(category_name[0])
            for results in list_results:
                key = [key for key in results]
                num = np.round_(results[key[0]], 3)
                results_file.write("        ->{0}:{1}\n".format(key[0], str(num)))


def save_results_to_csv_file(data_dictionary):
    """
    Method for writing dictionary of dictionaries to csv file.
    :param data_dictionary: data structure which consist of dictionary of dictionaries. 
    :return: None
    """
    with io.open(os.path.join(prop.FOLDER_RESULTS, 'results.csv'), 'a', encoding='utf8') as csvfile:
        fieldnames = ['SKU', 'ATTRIBUTE_NAME', 'ATRTIBUTE_MEAN', 'PROBABILITY']
        csv_writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        for elem in data_dictionary.keys():
            SKU = elem
            for dataList in data_dictionary[elem]:
                attr = [key for key in dataList][0]
                for values in dataList.values():
                    for dict in values:
                        for sub_attr, sub_attr_value in dict.items():
                            csv_writer.writerow({'SKU': SKU, 'ATTRIBUTE_NAME': attr, 'ATRTIBUTE_MEAN': sub_attr,
                                                'PROBABILITY': sub_attr_value})


def threshould_output(listResults):
    '''Method implements THRESHOLD LOGIC. To find out more, look at the file READEME.md.
        Input:
            listResults - type:list; every element of this list should be dicitionary
        Return:
            updeted list with Threshold logic.
    '''
    outputDict = []
    if prop.THRESHOLD_BOOLEAN == False:
        maxProbability = 0
        maxValueName = ''
        for pare in listResults:
            for attrName in pare.keys():
                if pare[attrName] > maxProbability:
                    maxProbability = pare[attrName]
                    maxValueName = attrName
        outputDict.append({maxValueName: maxProbability})
    if prop.THRESHOLD_BOOLEAN == True:
        for pare in listResults:
            for attrName in pare.keys():
                if pare[attrName] > float(prop.THRESHOLD_VALUE):
                    outputDict.append({attrName: pare[attrName]})
    return outputDict