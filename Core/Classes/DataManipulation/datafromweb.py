import logging
import time
import urllib.request as urllib

from Core.Configuration.properties import PATH_TO_PICTURES_FOLDER


def download_picture(url, path):
    """
    Method for downloading picture with using url
    """
    try:
        urllib.urlretrieve(url, path)
        logging.debug("Pictures had been downloaded from {0} and saved in: {1}".format(url, path))
    except ConnectionError:
        logging.error("Pictures from URL: {0} can't be downloaded".format(url))


def write_url_to_file(url, path):
    """
    Method for write url link into category description file
    """
    with open(path, 'a') as file:
        file.write(url + "\n")
    file.close()


def downloading_picture_process(url, category, waiting):
    """
    Method for downloading picture + saving results into file.
    Input: url
    """
    time.sleep(waiting)
    path_to_category_folder = PATH_TO_PICTURES_FOLDER + category.__str__() + "/{0}.txt".format(category)
    write_url_to_file(url, path_to_category_folder)
