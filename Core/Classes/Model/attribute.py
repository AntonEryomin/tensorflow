class Attribute():
    """
    Class for describing attributes, that
    """
    def __init__(self, name, subcategories='default'):
        self.name = name
        self.subcategories = subcategories

    def __str__(self):
        return str(self.name)
