import json
import logging
import os
import re
import time
import urllib.request as urllib

import requests

from Core.Configuration import properties as prop
from Core.SKU._sku import SKU


def get_parameters_from_string(data_string):
    """
    Method for parsing string on id and attributes values.
    :param data_string: string with id and attributes.
    :return: dictionary with id and attr as keys.
    """
    sku_dictionary = {}
    sku_dictionary['id'] = data_string[1:13]
    sku_dictionary['attr'] = json.dumps(data_string[16:-1], ensure_ascii=False)
    return sku_dictionary


def get_sku_list_json(path_to_file):
    # TODO Метод который нужно будет трансформировать после оптимизации объектной модели
    """
    Method for getting list SKU as list ot strings from json.
    :param path_to_file: path to file, where locate all information about SKU
    :return: return list SKU
    """
    list_string = []
    with open(path_to_file, 'r', encoding='utf-8') as f:
        try:
            for line in iter(f):
                line_json = str(line[16:])
                a = re.search(r'{.*\}', line_json)
                b = json.loads(a.group(0))
                list_string.append(SKU(str(line[1:13]), b))
                logging.info(SKU(str(line[1:13]), b))
            logging.debug("File {0} has been successfully parced".format(path_to_file))
        except IOError as io:
            logging.error("Can't find file {0}".format(path_to_file))
            logging.debug(io)
    return list_string


def get_sku_list_input(path_to_file):
    # TODO Метод который нужно будет трансформировать после оптимизации объектной модели
    """
    Method for getting list SKU as list ot strings from .txt file.
    :param path_to_file: path to file, where locate all information about SKU
    :return: return list SKU
    """
    list_string = []
    with open(path_to_file, 'r', encoding='utf-8') as f:
        try:
            for line in iter(f):
                a = re.search(r'([A-Z]|[a-z]|[0-9])\w+', str(line[0:13]))
                list_string.append(a.group(0))
            logging.debug("File {0} has been successfully parced".format(path_to_file))
        except IOError as io:
            logging.error("Can't find file {0}".format(path_to_file))
            logging.debug(io)
    return list_string


def get_sku_list_iter(path_to_file):
    # TODO Метод который нужно будет трансформировать после оптимизации объектной модели
    """
    Method for getting list SKU as list ot strings from .txt file.
    :param path_to_file: path to file, where locate all information about SKU
    :return: iterable object with SKU
    """
    with open(path_to_file, 'r', encoding='utf-8') as f:
        while True:
            line = f.readline().strip()[0:12]

            print(line)
            if not line:
                break
            yield line


def get_full_list_sku(category, dictionary):
    # TODO Метод который нужно будет удалить после оптимизации объектной модели
    """
    Method for getting list SKU for current category.
    :param category: category which using for searching SKU
    :param dictionary: information about SKU as dictionary where key is
    :return: list SKUs which correspond to category
    """
    list_sku = []
    for sku in dictionary:
        if category == str(dictionary[sku]):
            list_sku.append(sku)
    logging.info("SKU List was successfully formed.")
    return list_sku


def get_list_sku_from_folder(dir_path):
    # TODO Метод который нужно будет трансформировать после оптимизации объектной модели
    """
    Method for getting sku list from folder.
    :param dir_path: path to searched folder
    :return: list SKU names as string
    """
    temp_empty_list = []
    for file in os.listdir(dir_path):
        a = re.search(r'(^[A-Z]|[a-z]|[0-9])\w+', file)
        elem = a.group(0)[:-4]
        if elem not in temp_empty_list:
            temp_empty_list.append(elem)
    return temp_empty_list


def get_all_path_for_sku(dir, sku):
    # TODO Метод который нужно будет трансформировать после оптимизации объектной модели
    """
    Method for getting all paths to pictures which associate with SKU.
    :param dir: path to folder with pictures
    :param sku: SKU which will be search in directory
    :return: list of path to the SKU
    """
    path_list = []
    all_paths = os.listdir(dir)
    for path in all_paths:
        if sku in path:
            path_list.append(os.path.join(dir, path))
    return path_list


def transform_sku_string_to_obj(string_sku, sku_all_objects_list):
    # TODO Метод который нужно будет удалить после оптимизации объектной модели
    """
    Method for transforming SKU as string to SKU as object.
    :param string_sku: SKU as string
    :param sku_all_objects_list: list with all SKUs
    :return: SKU as object
    """
    for skuObj in sku_all_objects_list:
        if skuObj.skuid == string_sku:
            return skuObj


def transform_sku_string_list_to_objects(list_sku, list_all_sku):
    # TODO Метод который нужно будет удалить после оптимизации объектной модели
    """
    Method for transform list SKU where SKU as string into list SKU where SKU as object.
    :param list_sku: list of SKU where each element as string.
    :param list_all_sku: list all SKUs where each element as SKU class object.
    :return: List SKU where all elements are SKU class objects.
    """
    objects_list = []
    for strSKU in list_sku:
        a = transform_sku_string_to_obj(strSKU, list_all_sku)
        if a is not None:
            objects_list.append(transform_sku_string_to_obj(strSKU, list_all_sku))
    return objects_list


def transform_sku_objects_list_to_string(list_sku_objects):
    # TODO Метод который нужно будет удалить после оптимизации объектной модели
    str_list = []
    for objSKU in list_sku_objects:
        str_list.append(objSKU.skuid)
    return str_list


def check_sku_if_downloaded(sku_all_obj_list, sku_downloaded_list):
    """
    Method for checking which SKUs had been already downloaded.
    :param sku_all_obj_list: list of SKU as objects
    :param sku_downloaded_list: list of already downloaded SKUs
    :return: Empty list if all SKUs were downloaded, otherwise list of SKUs as objects which weren't downloaded.
    """
    sku_skip_list = []
    for sku in sku_all_obj_list:
        if sku.skuid not in sku_downloaded_list:
            sku_skip_list.append(sku)
    return sku_skip_list


def download_sku(sku_string):
    #TODO Переписать метод на использование SKU как объекта а не строки. Добавить обработку исключений если возникает проблем со скачиванием.
    """
    Method for download SKU.
    :param sku_string: SKU as string
    :return: None
    """
    try:
        json_to_work = requests.post(str(prop.APIGW_SERVER), data=json.dumps({'product_sku': sku_string})).text
        temp = json.loads(json_to_work)
        temporary_list = temp['product']['gallery']
        list_pictures = []
        for i in temporary_list:
            list_pictures.append(prop.PICTURE_SERVER + i)
        counter = len(list_pictures)

        if len(list_pictures) > 0:
            for url in list_pictures:
                picture_path = prop.RECOGNITION_PICTURES_FOLDER + sku_string + '_pic{0}.jpg'.format(counter)
                urllib.urlretrieve(url, picture_path)
                #time.sleep(prop.TIMEOUT_REQUESTS)
                counter = counter - 1
    except Exception:
        print(sku_string)


def get_sku_list_to_download(all_sku_list, downloaded_sku_list):
    #TODO Нужно переписать на использование только объектов
    """
    Method for getting list SKU for downloading.
    :param all_sku_list: list all SKUs
    :param downloaded_sku_list: list SKU which already downloaded
    :return: list SKU for downloading
    """
    return list(set(all_sku_list) - set(downloaded_sku_list))


def get_category_sku_dictionary(path_to_folder):
    """
    Method for creating dictionary where key is file name and value is list of SKUs.
    :param path_to_folder: path to folder
    :return: dictionary
    """
    dictionary_category_sku = {}
    file_list = os.listdir(path_to_folder)
    file_list.remove('.DS_Store')
    for file in file_list:
        dictionary_category_sku[file] = get_sku_list_input(path_to_folder + file)
    return dictionary_category_sku


def get_all_unique_categories(list_sku):
    """  
    :param list_sku: List with SKUs as object. 
    :return: Return list unique SKUs attributes. 
    """
    list_unique_categories = []
    for sku in list_sku:
        for key in sku.skuJSON:
            if key not in list_unique_categories:
                list_unique_categories.append(key)
    logging.debug("List with unique SKUs was successfully formed")
    return list_unique_categories


def get_all_unique_attr_values(sku_objects_list, attribute_name):
    """
    Method for getting all unique attribute values.
    :param sku_objects_list: list of SKUs as objects
    :param attribute_name: attribute name which will be searching
    :return: list of attribute values
    """
    unique_list = []
    for obj in sku_objects_list:
        attr_value = obj.get_attribute_value(attribute_name)
        if attr_value not in unique_list:
            if attr_value is not None:
                unique_list.append(attr_value)
    return unique_list


def get_attribute_name_from_file_name(file_path):
    file_name = file_path.rsplit('/')[-1]
    a = re.search(r'((?:(?!.txt|[0-9]).)*)', file_name)
    return a.group(0).lower()


# ----------------------------------------------------------------------------------------------------------------------
#TODO МОЖЕТ БЫТЬ ЭТОТ МЕТОД НУЖНО УДАЛИТЬ?
def checking_input_sku(list_downloaded_sku_object, list_sku_input, list_sku_all_object):
    """
    Comparing SKU getting from category.txt with SKU which were already downloaded.
    :param list_downloaded_sku_object: 
    :param list_sku_input: 
    :param list_sku_all_object: 
    :return: Tuple with two elements. The first element is list contains SKUs which contain in category.txt and are
    downloaded. The second list contains SKUs which aren't downloaded but contain in category.txt.
    """
    list_sku_as_objects = []
    list_missed_sku = []
    for skuObj in transform_sku_string_list_to_objects(list_sku=list_sku_input, list_all_sku=list_sku_all_object):
        if skuObj.id in list_downloaded_sku_object:
            list_sku_as_objects.append(skuObj)
        else:
            list_missed_sku.append(skuObj)
    return (list_sku_as_objects,list_missed_sku)
# ----------------------------------------------------------------------------------------------------------------------
