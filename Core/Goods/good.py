class Good:
    def __init__(self, good_attributes):
        self.good_attributes = good_attributes

    def get_important_attributes(self):
        important_attributes = {}
        try:
            important_attributes['sku'] = self.good_attributes['sku']
            important_attributes['type'] = self.good_attributes['type']
            important_attributes['name'] = self.good_attributes['name']
            important_attributes['attributes'] = self.good_attributes['attributes']
            important_attributes['online pictures path'] = self.good_attributes['gallery']
        except KeyError:
            important_attributes['sku'] = 'undefined'
            important_attributes['type'] = 'undefined'
            important_attributes['name'] = 'undefined'
            important_attributes['attributes'] = 'undefined'
            important_attributes['online pictures path'] = 'undefined'
        return important_attributes