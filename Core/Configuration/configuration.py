class Configuration:
    def __init__(self):
        self.db_host = '127.0.0.1'
        self.db_port = 27017
        self.db_name = 'Lamoda'
        self.apigw_url = r'http://apigw.lamoda.ru/json/get_product_by_sku'
        self.picture_server = r'http://pi1.lmcdn.ru/product'
        self.app_host = '127.0.0.1'
        self.app_port = 5000
        self.catalogue_server = r'http://catalog.services.lamoda.tech/products/'