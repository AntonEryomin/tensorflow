import json


class Recodnized_image:
    """
    Class for describing  already recognized image. Output result implements as JSON.
    """

    def __init__(self, file_name='non_name', product_type='non_type', product_type_probability='0',
                 product_subtype='non_subtype', product_subtype_probability='0', product_color='non_color',
                 product_color_probability='0'):
        self.file_name = file_name
        self.product_type = product_type
        self.product_type_probability = product_type_probability
        self.product_subtype = product_subtype
        self.product_subtype_probability = product_subtype_probability
        self.product_color = product_color
        self.product_color_probability = product_color_probability

    def to_json(self):
        json_data = {}
        json_data["file_name"] = self.file_name
        json_data["product_type"] = {"product_type": self.product_type,
                                     "product_type_probability": self.product_type_probability}
        json_data["product_subtype"] = {"product_subtype": self.product_subtype,
                                        "product_subtype_probability": self.product_subtype_probability}
        json_data["product_color"] = {"product_color": self.product_color,
                                      "product_color_probability": self.product_color_probability}
        return json.dumps(json_data)
