# -*- coding: utf-8 -*-
#TODO Проработать данный класс, и затем использовать его при обучении.
"""
Класс для создания различного рода трансформации с картинкой. Необходимо
"""
import os

from PIL import Image as im

from Core.Pictures.picture import Picture


class Transformation(Picture):
    def __init__(self):
        pass

    def rotating_picture(self, path_folder):
        """
        Method for rotating current picture and store them into folder. The picture will be rotated 3 times (90, 180 and
        270 degrees.)
        :param path_folder: path to folder where reflected images will be stored.
        """
        rotation_pool = [0, 90, 180, 270]
        for rotation in rotation_pool:
            temp_pic = im.open(self.txt_info_path).rotate(rotation)
            temp_pic.save(os.path.join(path_folder, os.path.join(path_folder,
                                                                 self.pic_name + '_' + str(
                                                                     rotation) + '.jpg')))

    def reflect_picture(self, path_folder):
        """
        Method for reflect image, will be used two params FLIP_RIGHT_LEFT to reflect around the vertical centerline and
        FLIP_TOP_BOTTOM to reflect around the horizontal centerline.
        :param path_folder: path to folder where reflected images will be stored.
        """
        im.open(self.txt_info_path).transpose(im.FLIP_LEFT_RIGHT).save(
            os.path.join(path_folder, os.path.join(path_folder, self.pic_name + '_FLIP_RIGHT_LEFT' + '.jpg')))
        im.open(self.txt_info_path).transpose(im.FLIP_TOP_BOTTOM).save(
            os.path.join(path_folder, os.path.join(path_folder, self.pic_name + '_FLIP_TOP_BOTTOM' + '.jpg')))