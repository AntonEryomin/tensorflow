from Core.Pictures.picture import Picture
from string import punctuation


class LamodaPicture(Picture):
    def __init__(self, path):
        super().__init__(path)
        self.sku = ''

    @property
    def sku(self):
        return self._sku

    @sku.setter
    def sku(self, s):
        if len(self.path) < 13:
            raise Exception("Lamoda picture path is too short.")
        sku = self.path.split(r'/')[-1][0:12]
        for char in punctuation:
            if char in sku:
                print(sku)
                raise Exception("Path has unallowable character: {0}".format(char))
        self._sku = sku
