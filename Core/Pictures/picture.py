"""
Class Picture.
Class object initialisation: set picture path. This class supports only these types of extensions: .jpg, .JPG, .jpeg,
.JPEG
"""
import os.path
import shutil


class Picture:
    def __init__(self, path):
        self.path = path

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, p):
        if not os.path.isfile(p):
            raise FileNotFoundError
        if os.path.splitext(p)[-1] not in ['.jpg', '.JPG', '.jpeg', '.JPEG']:
            raise Exception("File has incorrect exception, please check it.")
        self._path = p

    def copy(self, new_path):
        shutil.copyfile(self.path, new_path)

    def move(self, new_path):
        shutil.move(self.path, new_path)
        self.path = new_path

    def delete(self):
        try:
            os.remove(self.path)
        except:
            FileNotFoundError
