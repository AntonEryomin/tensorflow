# -*- coding: utf-8 -*-
"""
Класс, создающий описание к существующей картинке. Наследуются от класса Pictures.
Для создания экземпляра класса необходимо передать путь к картинки, к которой создаётся описание, а так же источник
данных, из которого они берутся.
"""
import os
import transliterate as tr
from Core.Pictures.picture import Picture
from Core.Configuration.properties import ROOT_LABELS_FOLDER_PATH
from Core.DataSource.data_source import DataSource


class PictureInfo(Picture):
    def __init__(self, pict_path, sku):
        pict_obj = Picture(pict_path)
        self.txt_info_path = os.path.join(ROOT_LABELS_FOLDER_PATH, pict_path.split('\\')[-1].split('.')[0] + '.txt')
        try:
            print(DataSource('catalogue', sku).get_data_info())
            self.data_from_source = DataSource('catalogue', pict_obj.get_pict_sku()).get_data_info()
            def get_name_from_data(json_data):
                try:
                    splitted_str = json_data['name'].split(' ')
                    if len(splitted_str) > 1:
                        full_name = ''
                        for name_part in splitted_str:
                            if tr.detect_language(name_part) == 'ru':
                                full_name = full_name + " {0}".format(name_part)
                        if full_name == 'кроссовкиulterra':
                            full_name = 'кроссовки'
                        return tr.translit(full_name.lstrip(), reversed=True).lower()
                    else:
                        return tr.translit(splitted_str[0], reversed=True).lower()
                except AttributeError:
                    print(pict_obj.get_pict_sku())
                except KeyError:
                    print(pict_obj.get_pict_sku())
                except TypeError:
                    print(pict_obj.get_pict_sku())

            def get_attr_from_data(json_data, attr_name):
                try:
                    data = json_data[attr_name]
                    if tr.detect_language(data) == 'ru':
                        return tr.translit(data, reversed=True).lower()
                    else:
                        return data.lower()
                except AttributeError:
                    return "can't find attribute {0}".format(attr_name.lower())
                except KeyError:
                    return "can't find attr {0} as key.".format(attr_name.lower())

            def get_subattr_from_data(json_data, subattr_name):
                try:
                    subattr = json_data['attributes'][subattr_name]
                    return tr.translit(subattr, reversed=True)
                except KeyError:
                    return tr.translit('без застежки', reversed=True)

            def get_heels_platform_type(json_data):
                """
                Метод для разбора значения каблука из каталога, на платформу и каблук.
                :param json_data:
                :return: возвращает список, где первый элемент это каблук, а второй элемент это платформа.
                """
                heels_platform = ["", ""]
                try:
                    hells_from_catalog = json_data['attributes']['type_of_heels'].split(",")
                    heels_list = ['без каблука', 'стандартный', 'шпилька']
                    platform_list = ['без платформы', 'платформа', 'танкетка']
                    for attr in hells_from_catalog:
                        if attr in heels_list:
                            heels_platform[0] = attr
                        elif attr in platform_list:
                            heels_platform[1] = attr
                    if heels_platform[0] is "":
                        heels_platform[0] = 'без каблука'
                    elif heels_platform[1] is "":
                        heels_platform[1] = 'без платформы'
                    heels_platform[0] = tr.translit(heels_platform[0], reversed=True)
                    heels_platform[1] = tr.translit(heels_platform[1], reversed=True)
                    return heels_platform
                except KeyError:
                    return [tr.translit('без каблука', reversed=True), tr.translit('без платформы', reversed=True)]
            self.name = get_name_from_data(self.data_from_source)
            self.type = get_attr_from_data(self.data_from_source, 'type')
            self.gender = get_attr_from_data(self.data_from_source, 'gender')
            self.claps = get_subattr_from_data(self.data_from_source, 'clasp')
            self.type_of_heels = get_heels_platform_type(self.data_from_source)[0]
            self.type_of_platform = get_heels_platform_type(self.data_from_source)[1]
        except TypeError:
            print(pict_path)
        except AttributeError:
            print(pict_path)

    def is_exist(self):
        """
        :return: True если файл  Метод для проверки существования файла с описанием к картинке существует, иначе False
        """
        if os.path.exists(self.txt_info_path):
            return True
        else:
            return False

    def get_data_from_file(self):
        """
        Получаем описание картинки из файла.
        :return: list of attributes
        """
        if self.is_exist():
            attributes_list = []
            with open(self.txt_info_path, 'r') as f:
                for line in f:
                    attributes_list.append(line.rstrip('\n'))
            return attributes_list
        else:
            return "Description file doesn't exist."

    def add_new_data(self, data_string):
        """
        Метод для добавления новых аттрибутов описания картинки в файл описания.
        :param data_string: строка, которую необходимо добавить.
        :return:
        """
        try:
            with open(self.txt_info_path, 'a') as f:
                f.write(data_string)
        except FileExistsError as e:
            print(e)

    def is_data_exist(self, checked_attribute):
        """
        Метод для проверки вхождения заданного атрибута в список атрибутов, описывающих картинку.
        :param checked_attribute: аттрибут, который необходимо проверить на вхождение.
        :return: True если аттрибут найден, иначе False.
        """
        try:
            if checked_attribute in self.get_data_from_file():
                return True
            else:
                return False
        except FileExistsError as e:
            print(e)

    def write_data_to_file(self):
        if len(self.__dict__) > 5:
            with open(self.txt_info_path, 'w') as f:
                for data in [self.name, self.gender, self.claps, self.type_of_heels, self.type_of_platform]:
                    f.write("{0}\n".format(data))
        else:
            print("can't write to {0}".format(self.txt_info_path))
