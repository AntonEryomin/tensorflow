Project description:
========================================
This project shows different types of image recognition approaches for solving tagging problem.



Project requirements:
========================================

1. Python 3.5.2
2. Tensorflow 1.4 (for better performance please compile your tensorflow package from source.
3. Pymongo
4. Flask
5. Scandir 1.6


How to run project:
========================================
Please use this steps for run only recognition module only:
1..... 

Please use this steps for run full application version:
1. Install mongodb. Please use this official instruction: https://docs.mongodb.com/manual/
2. Run your mongodb server.
