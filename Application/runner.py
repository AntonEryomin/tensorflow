from Core.Configuration.configuration import Configuration
from DataBase.database_main import MongoDataBase
from Application.AppForFashion.app import app


if __name__ == "__main__":
    config = Configuration()
    # mongo_db = MongoDataBase()
    # if not mongo_db.if_run():
    #     mongo_db.create()
    app.run(host=config.app_host, port=config.app_port, debug=True)