import os
from flask import render_template, request
from Application.AppForFashion.app import app


def get_list_models():
    path_to_models = r'/home/antoneryomin/tensorflow/tensorflow/Application/AppForFashion/app/static/models'
    print(os.listdir(path_to_models))


@app.route('/')
@app.route('/index')
def index():
    model = {'version': 0.1}
    return render_template(r"index.html", title='Home', model=model)


@app.route('/upload', methods=['POST'])
def upload_picture():
    file = request.files['image']
    f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
    file.save(f)