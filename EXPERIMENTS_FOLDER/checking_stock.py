# -*- coding: utf-8 -*-
"""
Скрипт, который на вход принимает файл со списком SKU, затем выкачивает их, после чего прогоняет их через модель
и записывает результаты в файл.
"""
from Core.Classes.Model.skuServices import get_sku_list_iter, download_sku


PATH_TO_TXT_FILE = r'/home/antoneryomin/tensorflow/tensorflow/EXPERIMENTS_FOLDER/sku_shoes.txt'

for sku in get_sku_list_iter(PATH_TO_TXT_FILE):
    download_sku(sku)