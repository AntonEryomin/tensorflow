import os
import scandir
from Core.Pictures.lamoda_picture import LamodaPicture
from Core.DataSource.data_source import DataSource
from Core.Goods.good import Good
from DataBase.database_main import MongoDataBase


for path, dirs, files in scandir.walk(r'/media/antoneryomin/Новый том/LAMODA PICTURES'):
    for file in files:
        try:
            pict_sku = LamodaPicture(os.path.join(path, file)).sku
            data_from_source = DataSource('catalogue', pict_sku).get_data_catalogue()
            good_data = Good(data_from_source).get_important_attributes()
            good_data['offline picture path'] = os.path.join(path, file)
            MongoDataBase().insert_document(good_data)
            # print(data_from_source)
        except:
            print(file)
            #os.remove(os.path.join(path, file))