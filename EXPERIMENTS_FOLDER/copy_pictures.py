# -*- coding: utf-8 -*-
"""
Временный скрипт, для подготовки картинок к обучению модели. Раскидывается на 7 папок:
3D - фотография обуви сбоку
bottom - фотография подошвы
front - фотография спереди
material - фотография материала
rear - фотография сзади
top - фотография сверху
double - фотография обоих ботинков
sidewall - фотография сбоку, она получается как бы чисто боковая

"""
import os
import shutil
from Core.Pictures.picture import Picture


PATH_TO_ROOT_FOLDER = r'/home/antoneryomin/experiments/Balance'
PATH_TO_FINAL_FOLDER = r'/media/antoneryomin/Новый том/to_new_model'


number_to_name = {'1': '3d', '2': 'rear', '3': 'bottom', '4': 'front', '5': 'sidewall', '6': 'toe',
                  '7': 'material', '9': 'model'}

for key, value in number_to_name.items():
    try:
        os.mkdir(os.path.join(PATH_TO_FINAL_FOLDER, value))
    except FileExistsError:
        pass


number = 0
for subfolder in os.listdir(PATH_TO_ROOT_FOLDER):
    for picture_name in os.listdir(os.path.join(PATH_TO_ROOT_FOLDER, subfolder)):
        pict_name_splitted = picture_name.split('_')
        if len(pict_name_splitted) == 2:
            picture_type_number = pict_name_splitted[-1][0]
            try:
                if len(os.listdir(os.path.join(PATH_TO_FINAL_FOLDER, number_to_name[picture_type_number]))) < 20000:
                    shutil.copyfile(os.path.join(PATH_TO_ROOT_FOLDER, subfolder, picture_name), os.path.join(PATH_TO_FINAL_FOLDER, number_to_name[picture_type_number], picture_name))
                    number += 1
            except KeyError:
                print(picture_name)

