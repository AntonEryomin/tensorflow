# -*- coding: utf-8 -*-
"""
Скрипт для подготовки данных исключительно для задачи EP. Делаем следующее: Берем только те SKU которые относятся к
обуви, те в поле type = "Shoes", Затем смотрим на поле Gender: если значение стоит woman, тогда выцепляем следующие
значения product name, а также attribute color_family и attribute type_of_heels. Если же стоит Gender man, тогда: берем
только 2 поля.
"""
import os
import requests
import time
import shutil
from Core.Pictures.picture import Picture
from Core.Pictures.picture_info import PictureInfo


start_folder = "D:\\for_test"
start_pict_descr_folder = "D:\\LAMODA PICTURES Training labels\\Shoes"
start_folder = "D:\\LAMODA PICTURES"
catalog_url = 'http://catalog.services.lamoda.tech/feed/products?country=ru'
training_root_folder = os.path.join("D:", "LAMODA PICTURES Training")


def get_all_picture_pathes(root_folder):
    """
    Метод для получения всех путей ко всем картинкам
    :param root_folder:
    :return:
    """
    pict_pathes = []
    for dir_1_lvl in os.listdir(root_folder):
        if os.path.isdir(os.path.join(root_folder, dir_1_lvl)):
            for dir_2_lvl in os.listdir(os.path.join(root_folder, dir_1_lvl)):
                if os.path.isdir(os.path.join(root_folder, dir_1_lvl, dir_2_lvl)):
                    for pict in os.listdir(os.path.join(root_folder, dir_1_lvl, dir_2_lvl)):
                        pict_pathes.append(os.path.join(root_folder, dir_1_lvl, dir_2_lvl, pict))
                else:
                    pict_pathes.append(os.path.join(root_folder, dir_1_lvl, dir_2_lvl))
        else:
            pict_pathes.append(os.path.join(root_folder, dir_1_lvl))
    return pict_pathes


def get_url(sku_name):
    return 'http://catalog.services.lamoda.tech/products/' + sku_name + '?country=ru'


def copy_picture(old_path, folder, subfolder, type_sku):
    new_path = os.path.join(training_root_folder, folder, subfolder, os.path.basename(old_path))
    if not os.path.exists(os.path.join(training_root_folder, folder, type_sku)):
        os.makedirs(os.path.join(training_root_folder, folder, type_sku))
    if not os.path.exists(os.path.join(training_root_folder, folder)):
        os.makedirs(os.path.join(training_root_folder, folder))
    if not os.path.exists(os.path.join(training_root_folder, folder, subfolder)):
        os.makedirs(os.path.join(training_root_folder, folder, subfolder))
    shutil.copyfile(old_path, new_path)


def get_sku_info(curent_sku):
    try:
        json_data = requests.get(get_url(curent_sku)).json()
        if json_data['data'] is not None:
            if json_data['data']['type'] == 'Shoes':
                if json_data['data']['gender'] == 'men':
                    if not os.path.exists(os.path.join(training_root_folder, 'men')):
                        os.mkdir(os.path.join(training_root_folder, 'men'))
                    if not os.path.exists(os.path.join(training_root_folder, 'men', json_data['data']['name'])):
                        os.mkdir(os.path.join(training_root_folder, 'men', json_data['data']['name']))
                    return os.path.join(training_root_folder, 'men', json_data['data']['name'])
                elif json_data['data']['gender'] == 'women':
                    if not os.path.exists(os.path.join(training_root_folder, 'women')):
                        os.mkdir(os.path.join(training_root_folder, 'women'))
                    if not os.path.exists(os.path.join(training_root_folder, 'women', json_data['data']['name'])):
                        os.mkdir(os.path.join(training_root_folder, 'women', json_data['data']['name']))
                    return os.path.join(training_root_folder, 'women', json_data['data']['name'])
            # for value in json_data['data'].keys():
            #     if json_data['data'][value] == 'Shoes' and json_data['data']['gender'] in ['men', 'woman']:
            #         data_dict['gender'] = json_data['data']['gender']
            #         data_dict['name'] = json_data['data']['name']
    except requests.exceptions.ConnectionError as err:
        print(err)


if __name__ == '__main__':
    unique_values = []
    for pict in get_all_picture_pathes(start_folder):
        changed_pict = Picture(pict)
        path = get_sku_info(changed_pict.sku)
        if path is not None:
            try:
                shutil.copy(src=pict, dst=os.path.join(path))
            except FileNotFoundError as e:
                print(e)
