# -*- coding: utf-8 -*-
"""
Скрипт для инициализации данных. Данные инициализируются следующим образом.
Шаг 1. Устанавливается корневая директория для картинок.
Шаг 2. Начинается обход по циклу по всем папкам с картинками, берется файл с картинкой и разбирается. На первом этапе
вытаскивается SKU для данной картинки. На втором этапе проверяется, существует ли файл описания для данной картинки,
если файла нет, тогда файл создается.
Шаг 3. Информация обо всех SKU, картинках соответсвтующих данному SKU, а также путях до текстовых файлах записываются
в отдельный текстовой файлик.
"""
import errno
import os
import transliterate as tr
from Core.Pictures.picture_info import PictureInfo
from Core.PictureData.picturedata import PictureData
from Core.Configuration.properties import ROOT_LABELS_FOLDER_PATH
from Core.Configuration.properties import ROOT_PICTURES_FOLDER_PATH
from Core.Pictures.picture import Picture


def get_sku_name_from_path(paht_to_pict):
    return os.path.basename(paht_to_pict).split('_')[0]


def get_all_picture_pathes(root_folder):
    """
    Метод для получения всех путей ко всем картинкам
    :param root_folder:
    :return:
    """
    pict_pathes = []
    for dir_1_lvl in os.listdir(root_folder):
        if os.path.isdir(os.path.join(root_folder, dir_1_lvl)):
            for dir_2_lvl in os.listdir(os.path.join(root_folder, dir_1_lvl)):
                if os.path.isdir(os.path.join(root_folder, dir_1_lvl, dir_2_lvl)):
                    for pict in os.listdir(os.path.join(root_folder, dir_1_lvl, dir_2_lvl)):
                        pict_pathes.append(os.path.join(root_folder, dir_1_lvl, dir_2_lvl, pict))
                else:
                    pict_pathes.append(os.path.join(root_folder, dir_1_lvl, dir_2_lvl))
        else:
            pict_pathes.append(os.path.join(root_folder, dir_1_lvl))
    return pict_pathes


def delete_sprite_pictures(list_all_pictures):
    """
    Метод для удаления всех картинок, содержащих sprite в названии.
    :param list_all_pictures: список всех путей до всех картинок.
    :return: Список путей до картинок, в которых не содержится слово sprite, так как данные картинки не подходят для
    обучения.
    """
    all_pictures_path = get_all_picture_pathes(ROOT_PICTURES_FOLDER_PATH)
    for path in all_pictures_path:
        if "sprite" in path:
            os.remove(path)


def is_label_file_exist(path_to_pict_path):
    """
    Метод для проверки существования файла с лейблами для картинки. Если такого файла не существует, тогда создаем его.
    Метод возвращает путь до файла.
    :param path_to_pict_path:
    :return:
    """
    checked_path = os.path.join(ROOT_LABELS_FOLDER_PATH + path_to_pict_path[len(ROOT_PICTURES_FOLDER_PATH):] + '.txt')
    if not os.path.exists(checked_path):
        try:
            os.makedirs(os.path.dirname(checked_path))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
        with open(checked_path, 'w') as f:
            f.write('')
    return checked_path


def transform_list_sku_obj_to_list_name(list_sku_obj):
    return [sku_name.id for sku_name in list_sku_obj]


def write_data_to_file(path_to_file, data_as_list):
    try:
        open(path_to_file, 'w').close()
        for attr in data_as_list:
            if tr.detect_language(attr) == 'ru':
                attr = tr.translit(attr, reversed=True)
            with open(path_to_file, 'a') as file:
                file.write(attr + '\n')
    except (OSError, IOError) as e:
        print(e)


def get_data_list_from_json(json_data):
    try:
        return [json_data['name'], json_data['type'], json_data['gender']]
    except:
        return ['no_name', 'no_type', 'no_gender']


if __name__ == '__main__':
    #TODO РЕШЕНИЕ ОЧЕНЬ ПЛОХОЕ И ТОЧНО БУДЕТ ТОРМОЗИТЬ, НУЖНО БУДЕТ В БУДУЩЕМ ЧТО_ТО С ЭТИМ ДЕЛАТЬ!!!
    list_sku_objects = []
    all_path_pictures = get_all_picture_pathes(ROOT_PICTURES_FOLDER_PATH)
    all_unique_labels = []
    for pic_path in all_path_pictures:
        picture = Picture(pic_path)
        pict_descr = PictureInfo(picture.txt_info_path)
        picture_data = PictureData(picture, pict_descr, 'catalogue')
        path_to_label = os.path.join(ROOT_LABELS_FOLDER_PATH, '{0}.txt'.format(os.path.basename(picture_data.picture.path)))
        if picture_data.get_picture_data_from_source()['data'] is not None:
            for label in picture_data.data_to_write():
                if label not in all_unique_labels:
                    all_unique_labels.append(label)
            picture_data.to_file(path_to_label)
            picture.move_picture(os.path.join('D:\\Lamoda_multilabels_pictures', os.path.basename(picture_data.picture.path)))
    with open('D:\\labels.txt', 'a') as f:
        for _ in all_unique_labels:
                if tr.detect_language(_) == 'ru':
                    _ = tr.translit(_, reversed=True)
                f.write(_ + '\n')
