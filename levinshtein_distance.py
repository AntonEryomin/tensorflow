# -*- coding: utf-8 -*-
"""
Класс определяющий множество слов, с которым сравнивается неизвестное слово. Для инициализации необходимо задать
исходное множество.
"""
import pyxdameraulevenshtein as damerleven


class Similarity:

    def __init__(self):
        #TODO нужно сделать универсальным способом сбора данных по фиксированным именам
        with open(r'/media/antoneryomin/Новый том/TEST/output_labels.txt') as f:
            basic_words_list = f.read().splitlines()

        self.basic_words_list = basic_words_list
        self.similarity_index = 0.3


    @staticmethod
    def words_similarity(word_1, word_2):
        """
        Проверяет схожесть двух слов по алгоритму Дамерау-Левинштейна с учетом длины слов.
        :param word_1:
        :param word_2:
        :return: возвращает значение схожести между двух слов
        """
        return round(damerleven.damerau_levenshtein_distance(word_1, word_2) / int(sum([len(word_2), len(word_1)]) / 2),
                     2)

    def returned_word(self, word):
        """
        Метод, который анализирует слово и возвращает это же слово, если ничего похожего в списках не найдено или же
        найдено точное совпадение, если же есть обнаружено похожее слово то возвращается оно.
        :param word:
        :return:
        """
        all_similarity = []
        for word_ in self.basic_words_list:
            all_similarity.append((word_, self.words_similarity(word_, word)))
        analyzed_similarity = ['', 10000000000000000000000000000.0]
        for based_word in self.basic_words_list:
            if self.words_similarity(based_word, word) == 0.0:
                return word
            elif self.words_similarity(word, based_word) < analyzed_similarity[1]:
                analyzed_similarity[0] = based_word
                analyzed_similarity[1] = self.words_similarity(word, based_word)
        if analyzed_similarity[1] < self.similarity_index:
            return analyzed_similarity[0]
        else:
            return word
