import unittest
import json
import os
import shutil
from PIL import Image
import Core.Pictures.picture

PATH_TO_CONFIG_FILE = '/home/antoneryomin/tensorflow/tensorflow/UnitTests/configuration_tests.json'


class PictureTest(unittest.TestCase):

    def setUp(self):
        with open(PATH_TO_CONFIG_FILE, 'r') as f:
            self.config = json.load(f)
        self.pictures_folder = os.path.join(self.config['root test folder'], self.config['pictures test folder'])
        Image.new('RGB', (800, 1200), (255, 255, 255)).save(os.path.join(self.pictures_folder, 'basic_picture.jpg'))
        self.picture = Core.Pictures.picture.Picture(os.path.join(self.config['root test folder'],
                                                                  self.config['pictures test folder'],
                                                                 'basic_picture.jpg'))
        self.temporary_folder = os.path.join(self.config['root test folder'], self.config['temporary test files folder'])
        os.mkdir(self.temporary_folder)

    def tearDown(self):
        shutil.rmtree(self.temporary_folder)

    def test___init__(self):
        with self.assertRaises(FileNotFoundError):
            self.picture.path = os.path.join(self.pictures_folder, 'basic_picture_.jpg')
        with self.assertRaises(Exception, msg='File has incorrect exception, please check it.'):
            self.picture.path = os.path.join(self.pictures_folder, 'extension_tests.png')

    def test_copy(self):
        self.picture.copy(os.path.join(self.temporary_folder, 'copied_picture.jpg'))
        self.assertEqual(len(os.listdir(self.temporary_folder)), 1)

    def test_move(self):
        self.picture.move(os.path.join(self.temporary_folder, 'moved_picture.jpg'))
        self.assertTrue(os.path.exists(os.path.join(self.temporary_folder, 'moved_picture.jpg')))
        self.assertEqual(self.picture.path, os.path.join(self.temporary_folder, 'moved_picture.jpg'))
        self.picture.move(os.path.join(self.pictures_folder, 'basic_picture.jpg'))

    def test_delete(self):
        self.picture.copy(os.path.join(self.temporary_folder, 'to_delete.jpg'))
        self.picture.delete()
        self.assertFalse(os.path.exists(self.picture.path))
        self.assertRaises(OSError, self.picture.delete())


if __name__ == '__main__':
    unittest.main()